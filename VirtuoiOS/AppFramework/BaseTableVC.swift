//
//  BaseTableVC.swift
//  Reach-Swift
//
//  Created by akash savediya on 12/08/17.
//  Copyright © 2017 Kartik. All rights reserved.
//

import UIKit

class BaseTableVC: BaseVC, UITableViewDelegate, UITableViewDataSource, CellActionListner {
    
    var list = [BaseHomeCell]();
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getCellView(indexPath: indexPath, tableView: tableView)
//        let model = list[indexPath.row];
//        cell.displayData(model: model)
//        cell.cellActionDelegate = self;
//        cell.position = indexPath.row;
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getCellHeight(indexPath: indexPath);
    }
    
    func getCellView(indexPath : IndexPath, tableView : UITableView)->UITableViewCell{
        let cellModel : BaseHomeCell = list[indexPath.row]
        let nibName : String = getCellNibName(cellType: cellModel.getCellType())
        let cell = getCellFromNibName(nibName: nibName)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell;
    }
    
    func getCellNibName(cellType : HOME_CELL_TYPE)->String{
        var nibName = "";
        switch cellType {
            default:
            break;
        }
        return nibName;
    }
    
    
    func getCellFromNibName(nibName : String)->UITableViewCell{
        let cell = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)?[0] as! UITableViewCell;
        return cell;
    }
    
    func getCellHeight(indexPath : IndexPath)->CGFloat{
        let cellModel : BaseHomeCell = list[indexPath.row]
        return getCellHeight(cellType: cellModel.getCellType())
    }
    
    func getCellHeight(cellType : HOME_CELL_TYPE)->CGFloat{
        switch cellType {
        default:
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You selected cell #\(indexPath.row)!")
        onCellClicked(indexPath : indexPath)
    }
    
    func onCellClicked(indexPath : IndexPath){
        
    }
    
    func onCellAction(actionType: CellAction, position: Int) {
        
    }
    
}
