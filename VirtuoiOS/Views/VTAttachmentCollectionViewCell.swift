//
//  VTAttachmentCollectionViewCell.swift
//  VirtuoiOS
//
//  Created by KiWontai on 3/6/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import AlamofireImage

class VTAttachmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var topView : UIView!
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var subTitleLabel : UILabel!
    @IBOutlet weak var buttonStackView : UIStackView!
    @IBOutlet weak var imageHeight : NSLayoutConstraint!
    var buttons : [UIButton] = [UIButton]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setAttachment(_ attachment : VTAttachmentItem) {
        if let attachArray = attachment.attachButtonDataArray, attachArray.count > 0 {
            self.buttons.removeAll()
            
            for v in buttonStackView.arrangedSubviews {
                self.buttonStackView.removeArrangedSubview(v)
                v.removeFromSuperview()
            }
            
            var i : Int = 0;
            for attach in attachArray {
                if i > 0 {
                    let v : UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: 0.5))
                    v.backgroundColor = UIColor.lightGray
                    self.buttonStackView.addArrangedSubview(v)
                }
                
                let btn = UIButton(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: 30))
                btn.setTitle(attach.buttonTitle, for: .normal)
                btn.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16)
                btn.setTitleColor(self.tintColor, for: .normal)
                btn.tag = i
                i += 1
                self.buttonStackView.addArrangedSubview(btn)
                buttons.append(btn)
            }
            
            self.titleLabel.text = attachment.title
            if let subtitle = attachment.subTitle {
                self.subTitleLabel.text = subtitle
                self.subTitleLabel.isHidden = false
            }
            else {
                self.subTitleLabel.isHidden = true
            }
            if let urlStr = attachment.imageURL, let imgURL = Foundation.URL(string: urlStr) {
                imageHeight.constant = 150.0
                self.imageView.af_setImage(withURL : imgURL)
            }
            else {
                imageHeight.constant = 1.0
            }
        }
    }
}
