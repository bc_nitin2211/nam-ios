//
//  JsonParser.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
class JsonParser: NSObject {
    
    static func parseJson(taskCode:TASKCODES, response : DataResponse<String>)->AnyObject{
        switch taskCode {
        case .GET_NAM_CONFIG:
            return NamConfigurationResponse.parseJson(json: response.result.value!)
        case .GET_USER_INFO:
            return UserInfoResponse.parseJson(json: response.result.value!)
        case .GET_ACCESS_TOKEN:
            return AccessTokenResponse.parseJson(json: response.result.value!)
        }
    }
    
    
    //    static func parseRegisterUserResponse(response : DataResponse<String>)->RegisterUserResponse{
    //        return RegisterUserResponse.parseResponse(response: response.result.value!);
    //    }
}
