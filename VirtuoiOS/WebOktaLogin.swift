//
//  WebOktaLogin.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 18/08/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import WebKit
class WebOktaLogin: BaseVC, UIWebViewDelegate {

    let HTML_QUERY = "document.getElementsByName('SAMLResponse')[0].value;";
    var config : NamConfiguration!
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let request = URLRequest.init(url: Foundation.URL.init(string: config.oktaLoginUrl)!)
        
        webView.delegate = self
        webView.loadRequest(request)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        webView.scrollView.contentInset = UIEdgeInsets.zero;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print(webView.request?.url?.absoluteString ?? "Default string")
        return true;
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print(webView.request?.url?.absoluteString ?? "Default string")
        let url = webView.request?.url?.absoluteString ?? "Default string";
        if url.contains(config.salesforceLoginUrl){
            print("URL contains salesforce login")
            if let response = webView.stringByEvaluatingJavaScript(from: HTML_QUERY){
                print("response\(response)")
                webView.stopLoading()
                goBackWithResponse(response: response);
            }else{
                print("Did not get response")
            }
        }
    }
    
    func goBackWithResponse(response : String){
        var params = [String:Any]();
        params.updateValue(response, forKey: "SAMLResponse")
        popOrDismissVC(params, reqCode: REQ_CODE.DEFAULT)
    }
}
