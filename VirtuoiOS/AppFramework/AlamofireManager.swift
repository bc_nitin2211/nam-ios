//
//  AlamofireManager.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import Alamofire
class AlamofireManager: NSObject {
    var httpRequest : HttpObject!
    var restDelegate : RestDelegate!
    init(httpRequest : HttpObject, restDelegate : RestDelegate) {
        self.httpRequest = httpRequest;
        self.restDelegate = restDelegate;
    }
    
    func startDownload(){
        restDelegate.onPreExecute(httpRequestObject: httpRequest, forTaskCode: httpRequest.strtaskCode)
        switch httpRequest.methodType {
        case METHODS.GET:
            getData()
        case METHODS.PUT:
            putData()
        case METHODS.DELETE:
            deleteData()
        case METHODS.POST:
            postData()
        case .PATCH:
            patchData()
            break;
        }
    }
    
    func getData(){
        let dataRequest : DataRequest = Alamofire.request(httpRequest.strUrl, method: .get, parameters: httpRequest.dicParams, encoding: URLEncoding.default, headers: httpRequest.dicHeaders)
        dataRequest.validate(statusCode: 200..<300).responseString {response in
            print("result:\(response.result.isSuccess)")
            print("value:\(String(describing: response.result.value))")
            switch response.result{
            case .success:
                self.onSuccessResponse(response: response);
                break
            case .failure:
                self.onFailureResponse(response: response);
                break
            }
        }
    }
    
    func onSuccessResponse(response : DataResponse<String>){
        print("SuccessData:\(String(describing: response.result.value))");
        let httpResponse = HttpResponse();
        httpResponse.responseObject = JsonParser.parseJson(taskCode: httpRequest.strtaskCode, response: response);
        httpResponse.responseResult = HttpResponseResult.SUCCESS;
        let flag = self.restDelegate.onSuccess(httpResponse, forTaskCode: httpRequest.strtaskCode, httpRequestObject: httpRequest);
        print("Success:\(flag)");
    }
    
    func onFailureResponse(response : DataResponse<String>){
        print("FailureData:\(String(describing: response.result.value))");
        let httpResponse = HttpResponse();
        httpResponse.responseResult = HttpResponseResult.FAILURE;
        self.restDelegate.onFailure(httpResponse, forTaskCode: httpRequest.strtaskCode)
    }
    
    
    func postData(){
        let dataRequest : DataRequest = Alamofire.request(httpRequest.strUrl, method: .post, parameters: httpRequest.dicParams, encoding: URLEncoding.default, headers: httpRequest.dicHeaders)
        dataRequest.validate(statusCode: 200..<300).responseString {response in
            print("result:\(response.result.isSuccess)")
            print("value:\(String(describing: response.result.value))")
            switch response.result{
            case .success:
                self.onSuccessResponse(response: response);
                break
            case .failure:
                self.onFailureResponse(response: response);
                break
            }
        }
    }
    
    func patchData(){
        let dataRequest : DataRequest = Alamofire.request(httpRequest.strUrl, method: .patch, parameters: httpRequest.dicParams, encoding: JSONEncoding.default, headers: httpRequest.dicHeaders)
        dataRequest.validate(statusCode: 200..<300).responseString {response in
            print("result:\(response.result.isSuccess)")
            print("value:\(String(describing: response.result.value))")
            switch response.result{
            case .success:
                self.onSuccessResponse(response: response);
                break
            case .failure:
                self.onFailureResponse(response: response);
                break
                
            }
        }
    }
    func deleteData(){
        
    }
    
    func putData(){
        
    }
}
