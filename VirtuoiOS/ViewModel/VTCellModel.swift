//
//  VTCellModel.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/6/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

public protocol VTCellModel {
    func cellForModel(_ tableView : UITableView) -> UITableViewCell
    func cellHeight(_ tableView : UITableView) -> CGFloat
}
