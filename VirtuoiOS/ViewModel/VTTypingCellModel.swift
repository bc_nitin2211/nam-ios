//
//  VTTypingCellModel.swift
//  VirtuoiOS
//
//  Created by KiWontai on 3/19/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

open class VTTypingCellModel: VTCellModel {
    
    var isTyping : Bool = false;
    
    public func cellForModel(_ tableView : UITableView) -> UITableViewCell {
        // TODO: cell
        let cell : VTWaitingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VTWaitingTableViewCell") as! VTWaitingTableViewCell
        if isTyping {
            cell.animationImageView.startAnimating()
        }
        else {
            cell.animationImageView.stopAnimating()
        }
        return cell
    }
    
    public func cellHeight(_ tableView : UITableView) -> CGFloat{
        if isTyping {
            return 44;
        }
        return 0.001
    }
}
