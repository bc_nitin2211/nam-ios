//
//  NamHomeVC.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 18/08/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class NamHomeVC: BaseVC {
    var config : NamConfiguration?
    var accessTokenResponse : AccessTokenResponse?
    var user : UserInfo?
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.isHidden = true;
        initViews();
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func startLoader() {
        self.activityIndicator.isHidden = false;
        self.activityIndicator.startAnimating()
    }
    
    override func stopLoader() {
        self.activityIndicator.isHidden = true;
        self.activityIndicator.stopAnimating()
    }
    
    func initViews(){
        if isLoggedIn(){
            setViewsIfLoggedIn()
            getUserInfo()
        }else{
            setViewIfLoggedOut()
        }
    }
    
    func setViewsIfLoggedIn(){
        loginBtn.setTitle("Sign Out", for: .normal)
        self.title = user?.name;
    }
    
    func setViewIfLoggedOut(){
        loginBtn.setTitle("Sign In", for: .normal)
        self.title = PARAMS.BOT_NAME;
    }
    
    func getNamConfig(){
        let req = ApiGenerator.getNamConfig();
        downloadData(httpRequest: req);
    }
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        let flag = super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest)
        
        switch taskcode {
        case .GET_NAM_CONFIG:
            onNamConfigResponseReceived(object: object)
            break;
        case .GET_ACCESS_TOKEN:
            let response = object.responseObject as! AccessTokenResponse
            if response.success!{
                self.accessTokenResponse = response;
                getUserInfo()
                //                    moveToBotPage()
            }else{
                showalert(title: "", message: response.getMessage())
            }
            break;
        case .GET_USER_INFO:
            let response = object.responseObject as! UserInfoResponse
            if response.success!{
                self.user = response.data
                setViewsIfLoggedIn()
                moveToBotPage();
            }else{
                showalert(title: "", message: response.getMessage())
                logOutUser()
                setViewIfLoggedOut()
            }
            break;
        }
        
        return flag
    }
    
    func moveToBotPage(){
        print("Move to bot page")
        let vc = VCUtil.getViewController(identifier: "ChatViewController") as! ChatViewController
        vc.subtitle = user?.name;
        vc.resultDelegate = self;
        let senderId = DefaultsUtil.getSenderId(self.config!.chatBotId)
        self.accessTokenResponse = AccessTokenResponse.getSavedInstance()
        vc.selectedBot = VTBotInfo(withDictionary: ["id" : self.config!.chatBotId,
                                                    "name": "News America Marketing",
                                                    "chatBotPlatformUserId": senderId,
                                                    "description": "Welcome to the News America Marketing Salesforce contacts bot.",
                                                    "status": "NEW",
                                                    "accessToken":self.accessTokenResponse!.accessToken,
                                                    "type":"SalesforceCRM",
                                                    "ownerId":self.user!.userId])
        VTBotInfo.initRunningInstance(vc.selectedBot)
        pushVC(vc: vc)
    }
    
    func onNamConfigResponseReceived(object : HttpResponse){
        let response = object.responseObject as! NamConfigurationResponse;
        if response.success!{
            if let data = response.data{
                if let config = data.enterpriseApplication{
                    self.config = config;
                    moveToWebOkta()
                }
            }
        }
    }
    
    func isLoggedIn()->Bool{
        if let response = NamConfigurationResponse.getSavedInstance(){
            if let data = response.data{
                if let config = data.enterpriseApplication{
                    self.config = config;
                    if let userInfoResponse = UserInfoResponse.getSavedInstance(){
                        self.user = userInfoResponse.data;
                        return true;
                    }
                    
                }
            }
        }
        return false;
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onLoginClicked(_ sender: Any) {
        if isLoggedIn(){
            logOutUser()
            setViewIfLoggedOut()
        }else{
            getNamConfig()
        }
    }
    
    func logOutUser(){
        DefaultsUtil.logoutUser()
    }
    
    func moveToWebOkta(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebOktaLogin") as! WebOktaLogin
        vc.resultDelegate = self;
        vc.config = self.config;
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func onVCResult(_ data: [String : Any]?, reqCode: REQ_CODE, resCode: RESULT_CODE) {
        if reqCode == REQ_CODE.DEFAULT{
            if let params = data{
                let samlResponse : String = params["SAMLResponse"] as! String
                getAccessToken(samlResponse)
            }
        }else if reqCode == REQ_CODE.SIGN_OUT{
            logOutUser()
            setViewIfLoggedOut()
        }
        
        
    }
    
    func getAccessToken(_ assertion : String){
        let req = ApiGenerator.getAccessToken(assertion: assertion, url: (self.config?.oauthEndPoint)!)
        downloadData(httpRequest: req);
    }
    
    func getUserInfo(){
        if let response = AccessTokenResponse.getSavedInstance(){
            let req = ApiGenerator.getUserInfo(response.accessToken, instanceUrl: response.instanceUrl);
            downloadData(httpRequest: req)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
