//
//  DefaultsUtil.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 18/08/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class DefaultsUtil: NSObject {
    static func isUserLoggedIn()->Bool{
        return UserDefaults.standard.bool(forKey: "isUserLoggedIn");
    }
    
    static func setUserLoggedIn(){
        UserDefaults.standard.set(true, forKey: "isUserLoggedIn");
    }
    
    static func logoutUser(){
        UserDefaults.standard.removeObject(forKey: "randomDeviceId")
        UserDefaults.standard.removeObject(forKey: "accessTokenResponse")
        UserDefaults.standard.removeObject(forKey: "namConfig")
        UserDefaults.standard.removeObject(forKey: "userResponse")
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn");
    }
    
    
    
    
    
    static func saveString(data : String, key : String){
        UserDefaults.standard.set(data, forKey: key);
    }
    
    static func getString(key : String)->String?{
        return UserDefaults.standard.string(forKey: key);
    }
    
    static func saveAccessTokenResponse(data : String){
        saveString(data: data  , key: "accessTokenResponse")
    }
    
    static func getAccessTokenResponse()->String?{
        return getString(key: "accessTokenResponse")
    }
    
    static func getNamConfig()->String?{
        return getString(key: "namConfig")
    }
    
    static func saveNamConfig(data : String){
        saveString(data: data  , key: "namConfig")
    }
    
    static func getUserResponse()->String?{
        return getString(key: "userResponse")
    }
    
    static func saveUserResponse(data : String){
        saveString(data: data  , key: "userResponse")
    }
    
    static func getRandomIdentifier()->String{
        if let randomString = UserDefaults.standard.string(forKey: "randomDeviceId"){
            print("randomDeviceId : \(randomString)")
            return randomString;
        }else{
            let random = randomString(12);
            saveString(data: random, key: "randomDeviceId");
            print("randomDeviceId : \(random)")
            return random;
        }
    }
    
    static func randomString(_ length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    static func getSenderId(_ botId : String)->String{
        return getRandomIdentifier()+botId
    }

}
