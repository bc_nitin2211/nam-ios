//
//  VTButtonTempleteTableViewCell.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/19/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTButtonTempleteTableViewCell: UITableViewCell {

    @IBOutlet var bgView : UIView!
    @IBOutlet var txtBgView: UIView!
    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var stackView : UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
