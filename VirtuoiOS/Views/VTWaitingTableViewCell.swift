//
//  VTWaitingTableViewCell.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/19/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTWaitingTableViewCell: UITableViewCell {

    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var animationImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let image = UIImage(named: "lbubble")!.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 10), resizingMode: .stretch)
        
        backgroundImageView.image = image
        
        self.animationImageView.animationImages = [UIImage(named:"waiting0")!, UIImage(named:"waiting1")!, UIImage(named:"waiting2")!];
        self.animationImageView.animationDuration = 1.0;
        self.animationImageView.animationRepeatCount = NSIntegerMax;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
