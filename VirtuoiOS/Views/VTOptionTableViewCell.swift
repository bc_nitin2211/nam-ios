//
//  VTOptionTableViewCell.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/4/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

protocol VTOptionTableViewCellDelegate : class {
    func touchOptionButton(_ index : Int)
}

let margin : CGFloat = CGFloat(44)

class VTOptionTableViewCell: UITableViewCell , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView : UICollectionView!
    var attachments : [VTAttachmentItem]?
    weak var delegate : VTOptionTableViewCellDelegate?
    var currentOffset : CGPoint = CGPoint()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.isPagingEnabled = false;
        
        self.collectionView.register(UINib(nibName: "VTAttachmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VTAttachmentCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAttachments(_ attachments : [VTAttachmentItem]?) {
        self.attachments = attachments
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
        self.collectionView.reloadData()
        currentOffset = self.collectionView.contentOffset
    }
    
    // MARK: - UICollectionView Delegate, Data Source, FlowLayout
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let attachments = self.attachments, attachments.count > 0 {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.attachments!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : VTAttachmentCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VTAttachmentCollectionViewCell", for: indexPath) as! VTAttachmentCollectionViewCell
        let attachment = attachments![indexPath.row]
        cell.setAttachment(attachment)
        var i = 0
        for btn in cell.buttons {
            btn.tag = indexPath.row * 100 + i
            i += 1
            btn.removeTarget(self, action: #selector(optionButtonClicked(_:)), for: .touchUpInside)
            btn.addTarget(self, action: #selector(optionButtonClicked(_:)), for: .touchUpInside)
        }
        
        cell.topView.layer.cornerRadius = 8.0
        cell.topView.layer.borderColor = UIColor.lightGray.cgColor
        cell.topView.layer.borderWidth = 0.5
        cell.topView.layer.shadowColor = UIColor.lightGray.cgColor;
        cell.topView.layer.shadowOpacity = 1.0
        cell.topView.layer.shadowRadius = 0.2
        cell.topView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        cell.topView.clipsToBounds = true;
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let attachment = attachments![indexPath.row]
        let count = attachment.attachButtonDataArray?.count
        var basicHeight = 30
        if let urlStr = attachment.imageURL, let _ = Foundation.URL(string: urlStr) {
            basicHeight = 190
        }
        if let sTitle = attachment.subTitle, sTitle.characters.count > 0 {
            basicHeight += 18
        }
        return CGSize(width: collectionView.frame.size.width - 88, height: CGFloat(attachment.maxHeight))
//        return CGSize(width: collectionView.frame.size.width - 88, height: CGFloat(basicHeight + (attachment.attachButtonDataArray?.count)! * 40))
    }
    
    func collectionView(_ collectionView: UICollectionView, targetContentOffsetForProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        if proposedContentOffset.x > currentOffset.x + 20 {
            currentOffset = CGPoint(x: currentOffset.x + collectionView.frame.size.width - 30, y: proposedContentOffset.y)
        }
        else if proposedContentOffset.x < currentOffset.x - 20 {
            currentOffset = CGPoint(x: currentOffset.x - collectionView.frame.size.width + 30, y: proposedContentOffset.y)
        }
        
        return currentOffset
    }
    
    func optionButtonClicked(_ sender : UIButton) {
        self.delegate?.touchOptionButton(sender.tag)
    }
}
