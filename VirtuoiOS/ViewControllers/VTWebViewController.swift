//
//  VTWebViewController.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/12/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTWebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet open var webView : UIWebView!
    @IBOutlet open var activityIndicator : UIActivityIndicatorView!
    
    open var webURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let urlStr = webURL, let url = Foundation.URL(string: urlStr) {
            let request = URLRequest(url: url)
            self.webView.loadRequest(request)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - WebView Delegate
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicator.isHidden = true
        self.activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.isHidden = true
        self.activityIndicator.startAnimating()
    }
    
    
}
