//
//  TokenResponse.swift
//  BrewBound
//
//  Created by Nitin Bansal on 24/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import ObjectMapper

class TokenResponse:BaseMapperModel {
    var data: [TokenData]?
    var message : String?
    var error : Bool?
    
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        error <- map["error"]
        message <- map["message"]
    }
}

class TokenData:BaseMapperModel{
    var serialNo : String?
    var tokenKey : String?
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        serialNo <- map["SerialNo"]
        tokenKey <- map["TokenDecrypt"]
    }
}
