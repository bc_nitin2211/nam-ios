//
//  NamConfigurationResponse.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 18/08/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import ObjectMapper
class NamConfigurationResponse: BaseResponse {

    var data : NamConfigSuccessResult?
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["successResult"]
    }
    
    static func getSavedInstance()->NamConfigurationResponse?{
        if let json = DefaultsUtil.getNamConfig(){
            let response = NamConfigurationResponse(JSONString : json)!
            return response;
        }
        return nil;
    }
    
    static func parseJson(json : String)->NamConfigurationResponse{
        let response = NamConfigurationResponse(JSONString : json)!
        if response.success!{
            if let d = response.data{
                if let _ = d.enterpriseApplication{
                    DefaultsUtil.saveNamConfig(data : json)
                }
            }
        }
        return response;
    }
}

class NamConfigSuccessResult: BaseMapperModel {
    var enterpriseApplication : NamConfiguration?
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        enterpriseApplication <- map["enterpriseApplication"];
    }
}

class NamConfiguration : BaseMapperModel{
    var oktaLoginUrl : String!
    var salesforceLoginUrl : String!
    var oauthEndPoint : String!
    var chatBotId : String!
    
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        oktaLoginUrl <- map["oktaLoginUrl"]
        salesforceLoginUrl <- map["salesforceLoginUrl"]
        oauthEndPoint <- map["oAuthEndPoint"]
        chatBotId <- map["chatBotId"]
//        oktaLoginUrl = PARAMS.OKTA_LOGIN_URL
//        salesforceLoginUrl = PARAMS.SALESFORCE_LOGIN
        oktaLoginUrl = salesforceLoginUrl;
//        oauthEndPoint = PARAMS.OAUTH_END_POINT
    }
}
