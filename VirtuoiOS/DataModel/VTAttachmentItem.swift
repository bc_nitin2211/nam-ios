//
//  VTAttachmentItem.swift
//  VirtuoiOS
//
//  Created by KiWontai on 3/6/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import Foundation

open class VTAttachmentItem {
    open var attachButtonDataArray : [VTAttachmentButtonData]? = nil
    open var imageURL: String?
    open var title : String!
    open var subTitle : String?
    open var maxHeight = 0;
    open var cellHeight = 0;
    
    
    init(withDictionary dict : [String: Any]) {
        title = dict["title"] as! String
        subTitle = dict["subtitle"] as? String
        imageURL = dict["image_url"] as? String
        if let buttons = dict["buttons"] as? [[String: Any]], buttons.count > 0 {
            attachButtonDataArray = VTAttachmentButtonData.parseAttachmentButtonData(buttons)
        }
    }
}
