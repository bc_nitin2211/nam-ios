//
//  VTBotInfo.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 2/28/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

open class VTBotInfo {
    
    static open var runningInstance : VTBotInfo!
    
    open let botID : String
    open let name : String
    open let botPlatfromUserId : String
    open let botDescription : String
    open let status : String?
    open let imageURLString : String?
    open let accessToken : String?
    open let type : String?
    open let ownerId : String?
    open var isReadOutOn = true;
    
    init(withDictionary dict : [String:Any]) {
        self.botID = dict["id"] as! String
        self.name = dict["name"] as! String
        self.botPlatfromUserId = dict["chatBotPlatformUserId"] as! String
        self.botDescription = dict["description"] as! String
        self.status = dict["status"] as? String
        self.imageURLString = dict["imageURL"] as? String
        self.accessToken = dict["accessToken"] as? String
        self.type = dict["type"] as? String
        self.ownerId = dict["ownerId"] as? String
    }
    
    static func initRunningInstance(_ instance : VTBotInfo){
        runningInstance = instance;
    }
    
    static func getRunningInstance()->VTBotInfo{
        return runningInstance;
    }
    
    static func getAccessTokensForNam()->[[String:Any]]{
        var accessTokens = [[String: Any]]()
        var accessToken = [String:Any]()
        accessToken.updateValue("SalesforceCRM", forKey: "name")
        if let response = AccessTokenResponse.getSavedInstance(){
            accessToken.updateValue(response.accessToken, forKey: "value")
            if let userResponse = UserInfoResponse.getSavedInstance(){
                if let user = userResponse.data{
                    accessToken.updateValue(user.userId, forKey: "ownerId")
                }
            }
        }
        accessTokens.append(accessToken);
        return accessTokens
    }
    
    
    func getAccessTokensArray()->[[String:Any]]{
        var accessTokens = [[String: Any]]()
        var accessToken = [String:Any]()
        accessToken.updateValue(self.type!, forKey: "name")
        accessToken.updateValue(self.accessToken!, forKey: "value")
        accessToken.updateValue(self.ownerId!, forKey: "ownerId")
        accessTokens.append(accessToken);
        return accessTokens
    }
}
