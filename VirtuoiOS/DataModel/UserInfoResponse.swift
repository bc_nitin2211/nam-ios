//
//  UserInfoResponse.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 18/08/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import ObjectMapper
class UserInfoResponse: BaseResponse {
    var message : String!
    var data : UserInfo?
    required init?(map: Map){
        super.init(map: map);
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        message <- map["message"]
        if data != nil{
            success = true;
        }else{
            success = false;
        }
    }
    
    override func getMessage() -> String {
        return message
    }
    
    static func getSavedInstance()->UserInfoResponse?{
        if let json = DefaultsUtil.getUserResponse(){
            let response = UserInfoResponse(JSONString : json)!
            return response;
        }
        return nil;
    }
    
    static func parseJson(json : String)->UserInfoResponse{
        let response = UserInfoResponse(JSONString : json)!
        if response.success!{
            DefaultsUtil.saveUserResponse(data: json)
        }
        return response;
    }
}

class UserInfo : BaseMapperModel{
    var name : String!
    var email : String!
    var username : String!
    var userId : String!
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        username <- map["username"]
        userId <- map["userId"]
        name <- map["name"]
        email <- map["email"]
    }
    
}
