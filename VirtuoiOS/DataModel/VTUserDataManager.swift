//
//  UserDataManager.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/3/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import Foundation

open class VTUserDataManager {
    static open let manager : VTUserDataManager = VTUserDataManager()
    
    open var selectedBotID : String! = ""
    
    open var userId : String {
        get {
            guard let uId = UserDefaults.standard.object(forKey: "UserId") as? String else {
                let newId = UUID().uuidString + "-" + selectedBotID
                self.userId = newId
                return newId
            }
            return uId
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "UserId")
            UserDefaults.standard.synchronize()
        }
    }
}
