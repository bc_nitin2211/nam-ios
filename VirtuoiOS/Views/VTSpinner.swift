//
//  VTSpinner.swift
//  VirtuoiOS
//
//  Created by KiWontai on 6/11/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTSpinner {
    
    static let manager: VTSpinner = VTSpinner()

    let spinnerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    let activiyIndicatorView : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    init() {
        spinnerView.addSubview(activiyIndicatorView)
        activiyIndicatorView.center = spinnerView.center
        activiyIndicatorView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
    }

    func start(inView view: UIView? = nil) {
        if let v = view {
            spinnerView.frame = v.bounds
            v.addSubview(spinnerView)
        }
        else {
            if let appDelegate = UIApplication.shared.delegate, let w = appDelegate.window, let bound = w?.bounds {
                spinnerView.frame = bound
                w?.addSubview(spinnerView)
            }
            else {
                
            }
        }
        
        activiyIndicatorView.startAnimating()
    }
    
    func stop() {
        activiyIndicatorView.stopAnimating()
        self.spinnerView.removeFromSuperview()
    }
}
