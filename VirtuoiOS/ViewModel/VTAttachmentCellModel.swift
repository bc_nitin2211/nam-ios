//
//  VTAttachmentCellModel.swift
//  VirtuoiOS
//
//  Created by KiWontai on 3/6/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import Foundation
protocol VTAttachmentCellModelDelegate : class {
    func touchAttachmentOptionButton(_ attachment : VTAttachmentItem, index : Int)
}

class VTAttachmentCellModel : VTCellModel, VTOptionTableViewCellDelegate {
    
    var attachments : [VTAttachmentItem]!
    weak var delegate : VTAttachmentCellModelDelegate?
    
    init(withAttachments attachments : [VTAttachmentItem]) {
        self.attachments = attachments
    }
    
    func cellForModel(_ tableView : UITableView) -> UITableViewCell {
        let cell : VTOptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VTOptionTableViewCell") as! VTOptionTableViewCell
        cell.setAttachments(attachments)
        cell.delegate = self;
        if let layout = cell.collectionView.collectionViewLayout as? VTAttachmentCollectionFlowLayout {
            let offset = cell.collectionView.contentOffset
            layout.currentOffset = offset
        }
        
        
        return cell;
    }
    
    func cellHeight(_ tableView: UITableView) -> CGFloat {
        var max = 0
        for attach in attachments {
            var basicHeight = 30
            if let urlStr = attach.imageURL, let _ = Foundation.URL(string: urlStr) {
                basicHeight = 190
            }
            if let sTitle = attach.subTitle, sTitle.characters.count > 0 {
                basicHeight += 18
            }
            let btnsHeight = (attach.attachButtonDataArray?.count)! * 40
            
            if basicHeight + btnsHeight > max {
                max = basicHeight + btnsHeight
            }
            attach.cellHeight = basicHeight + btnsHeight;
        }
        
        for attach in attachments{
            attach.maxHeight = max
        }
        return CGFloat(max)
    }
    
    func touchOptionButton(_ index: Int) {
        let attachment = attachments[index/100]
        let btnIndex = index % 100
        self.delegate?.touchAttachmentOptionButton(attachment, index: btnIndex)
    }
}
