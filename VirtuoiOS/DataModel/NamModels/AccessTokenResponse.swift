//
//  AccessTokenResponse.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 18/08/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import ObjectMapper
class AccessTokenResponse :  BaseResponse {
    var accessToken : String!
    var instanceUrl : String!
    required init?(map: Map){
        super.init(map: map);
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        accessToken <- map["access_token"]
        instanceUrl <- map["instance_url"]
        if accessToken != nil{
            success = true;
        }else{
            success = false;
        }
    }
    
    static func getSavedInstance()->AccessTokenResponse?{
        if let json = DefaultsUtil.getAccessTokenResponse(){
            let response = AccessTokenResponse(JSONString : json)!
            return response;
        }
        return nil;
    }
    
    static func parseJson(json : String)->AccessTokenResponse{
        let response = AccessTokenResponse(JSONString : json)!
        if response.success!{
            DefaultsUtil.saveAccessTokenResponse(data : json)
        }
        return response;
    }

}
