//
//  ApiGenerator.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import Alamofire
class ApiGenerator: NSObject {
    
    static func login(username: String, password: String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        //        httpRequest.methodType = METHODS.POST;
        //        httpRequest.strUrl = URL.LOGIN_USER;
        //        httpRequest.strtaskCode = TASKCODES.LOGIN;
        //        httpRequest.addParam("username", andValue: username)
        //        httpRequest.addParam("password", andValue: password)
        //        httpRequest.setPostMethod();
        return httpRequest;
    }
    
    static func getNamConfig()->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = URL.NAM_CONFIG;
        httpRequest.strtaskCode = TASKCODES.GET_NAM_CONFIG;
        httpRequest.addHeader("Content-Type", value: "application/json")
        httpRequest.addParam("type", andValue: "SalesforceCRM")
        httpRequest.addParam("chatBotId", andValue: PARAMS.CHAT_BOT_ID)
//        httpRequest.setPostMethod();
        return httpRequest;
    }
    
    static func getAccessToken(assertion : String, url : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.POST;
        httpRequest.strUrl = url;
        httpRequest.strtaskCode = TASKCODES.GET_ACCESS_TOKEN;
        httpRequest.addHeader("Content-Type", value: "application/x-www-form-urlencoded")
        httpRequest.addParam("grant_type", andValue: PARAMS.GRANT_TYPE)
        httpRequest.addParam("assertion_type", andValue: URLEncodedString(text: PARAMS.ASSERTION_TYPE) ?? "")
        httpRequest.addParam("assertion", andValue: URLEncodedString(text: assertion) ?? "")
        return httpRequest;
    }
    
    static func getUserInfo(_ accessToken : String, instanceUrl : String)->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = URL.USER_INFO;
        httpRequest.strtaskCode = TASKCODES.GET_USER_INFO;
        httpRequest.addHeader("accessToken", value: accessToken)
        httpRequest.addHeader("instanceUrl", value: instanceUrl)
        return httpRequest;
    }
    
    static func URLEncodedString(text : String) -> String? {
        return text;
//        let escapedString = text.addingPercentEncoding(withAllowedCharacters: .urlPasswordAllowed)
////        print("Encoded String : \(String(describing: escapedString))")
//        print("URLEncoded Alamofire String : \(URLEncoding.default.escape(text))")
//        return escapedString
    }
    
    
    
}
