//
//  VTAttachmentCollectionFlowLayout.swift
//  VirtuoiOS
//
//  Created by KiWontai on 3/8/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTAttachmentCollectionFlowLayout: UICollectionViewFlowLayout {
    var currentOffset : CGPoint = CGPoint(x: -44, y: 0)
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let cv = self.collectionView {
            if proposedContentOffset.x > currentOffset.x + 100 {
                currentOffset = CGPoint(x: currentOffset.x + cv.frame.size.width - 78, y: proposedContentOffset.y)
            }
            else if proposedContentOffset.x < currentOffset.x - 100 {
                currentOffset = CGPoint(x: currentOffset.x - cv.frame.size.width + 78, y: proposedContentOffset.y)
            }
        }
        
        return currentOffset
    }
}
