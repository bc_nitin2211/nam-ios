//
//  BaseResponse.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 18/08/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import ObjectMapper
class BaseResponse: BaseMapperModel {

    var success : Bool?
    required init?(map: Map){
        super.init(map: map);
    }

    func getMessage()->String{
        return "Oops some unknown error occurred";
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        success <- map["success"]
    }
}
