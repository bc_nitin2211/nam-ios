//
//  VTNetworkManager.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 2/28/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import Alamofire
import SocketIO

protocol VTNetworkManagerDelegate : class {
    func websocketDidConnect(_ manager : VTNetworkManager)
    func websocketDidReceive(_ data: [Any])
}

class VTNetworkManager : NSObject  {
    static open let manager : VTNetworkManager = VTNetworkManager();
    
    open let reachability = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    open var socket : SocketIOClient?
    
    var strBaseURL : String = "https://platformservice.botcentralapi.com"
    var msgBaseURL : String = "https://msg.botcentralapi.com"
    var strPlatformVersion : String = "bot-platform-manager-0.1"
    var strRESTSocketVersion : String = "socket.io/"
    var socketURL : String = "https://msg.botcentralapi.com/socket.io"
    
    open var botInfo : VTBotInfo?
    open var botSId : String?
    
    open var isReachable : Bool {
        get {
            if let reachable = reachability {
                return reachable.isReachable;
            }
            return false;
        }
    }
    
    weak open var delegate : VTNetworkManagerDelegate?
    
    override init() {
        super.init()
        setupReachability()
    }
    
    deinit {
        self.socket?.disconnect()
    }
    
    open func setupReachability() {
        reachability?.listener = { status in
            switch status {
            case .notReachable:
                print("Not Reachable")
            case .reachable(.ethernetOrWiFi):
                print("wifi")
            case .reachable(.wwan):
                print("wwan")
            default: // Unknown
                print("unknown")
            }
        }
    }
    
    open func connectSocket(withURL url: String?, /*withSId sId : String,*/ withCompletion completion: @escaping (_ error: NSError)->Void ) {
        if let socket = self.socket {
            socket.disconnect()
        }
        if let urlString = url {
            self.socketURL = urlString
        }
        
        let finalURL = self.socketURL
        //+ "/?userId=" + VTUserDataManager.manager.userId + "&EID=3&transport=websocket&sid=" + sId;
        
        self.socket = SocketIOClient(socketURL: Foundation.URL(string : finalURL)!, config: [.log(false), .forcePolling(true)])
        
        self.socket?.on("connect") {data, ack in
            print("socket connected")
            if (self.delegate != nil) {
                self.delegate?.websocketDidConnect(self)
            }
        }
        
        self.socket?.on("usermessage") {data, ack in
            print("get usermessage")
        }
        
        self.socket?.on("botresponse") {data, ack in
            print("get bot response")
            if let d = self.delegate {
                d.websocketDidReceive(data)
            }
        }
        
        self.socket?.on("agentresponse") {data, ack in
            print("get bot response")
            if let d = self.delegate {
                d.websocketDidReceive(data)
            }
        }
        
        self.socket?.connect()
        
//        self.socket?.connect(timeoutAfter: 3, withHandler: {
//            completion(NSError(domain: "Virtuo", code: 900, userInfo: nil))
//        })
    }
    
    open func getBotList(completion: @escaping (_ botList : [VTBotInfo]?, _ error: NSError?)-> Void) {
        VTRestAPI.GET(withBaseURL: strBaseURL, withServiceNVersion: strPlatformVersion, withPath: "/botinfo/bot/demolist", withParams: nil) { (data: Data?, error: NSError?) in
            if let _ = error {
                completion(nil, NSError(domain: "NETWORK", code: 901, userInfo: nil))
                return;
            }
            if let d = data {
                do {
                    let jsonObject = try JSONSerialization.jsonObject(with: d, options: [])
                    if let responseDict = jsonObject as? Dictionary<String, Any>, let infoData = responseDict["successResult"] as? [String: Any], let botData = infoData["demoBots"] as? [[String:Any]] {
                        
                        var demoBots = [VTBotInfo]()
                        
                        for bot in botData {
                            let botInfo = VTBotInfo(withDictionary: bot)
                            demoBots.append(botInfo)
                        }
                        
                        completion(demoBots, nil)
                    }
                    else {
                        completion(nil, NSError(domain: "NET_ERROR", code: 900, userInfo: nil))
                    }
                }
                catch {
                    completion(nil, NSError(domain: "NET_ERROR", code: 900, userInfo: nil))
                }
            }
        }
    }
    
    /**
     Get Session Id, before connecting chat bot, call this and then get sid
    */
    open func getSession(completion: @escaping (_ sessionId : String?, _ error : NSError?) -> Void) {
        let params : [String: Any] = ["userId" : VTUserDataManager.manager.userId, "EIO": 3, "transport": "polling"]
        VTRestAPI.GET(withBaseURL: msgBaseURL, withServiceNVersion: strRESTSocketVersion, withPath: nil, withParams: params) { (data : Data?, error : NSError?) in
            if let _ = error {
                completion(nil, NSError(domain: "NETWORK", code: 901, userInfo: nil))
                return;
            }
            if let d : Data  = data , let s = String(data: d, encoding: .ascii) {
                let sIndex : Range<String.Index> = s.range(of: "{")!
                let substr = s.substring(from: sIndex.lowerBound)
                do {
                    let jobj = try JSONSerialization.jsonObject(with: substr.data(using: .utf8)!)
                    if let json = jobj as? [String : Any] {
                        let sid = json["sid"] as? String
                        completion(sid, nil)
                    }
                }
                catch {
                    completion(nil, NSError(domain: "NETWORK", code: 901, userInfo: nil))
                }
            }
            else {
                completion(nil, NSError(domain: "NETWORK", code: 901, userInfo: nil))
            }
        }
    }
    
    open func getSocket(withSessionId sId : String, completion: @escaping (_ session : String?, _ error : Error?) -> Void) {
        let params = ["userId": VTUserDataManager.manager.userId,
                      "EIO": 3,
                      "transport": "polling",
                      "sid": sId] as [String : Any]
        VTRestAPI.GET(withBaseURL: msgBaseURL, withServiceNVersion: strRESTSocketVersion, withPath: nil, withParams: params) { (data : Data?, error : NSError?) in
            // TODO
            completion(nil, error)
        }
    }
    
    
    open func getBotInformation(completion: @escaping (_ botInfo : VTBotInfo? , _ error : NSError?) -> Void ) {
        VTRestAPI.GET(withBaseURL: strBaseURL, withServiceNVersion: strPlatformVersion, withPath: "botinfo/bot/b97bbda4d96a0bb98d7c6d31486cff593e1dfbe1", withParams: nil) { (data : Data?, error : NSError?) in
            
            if let d = data {
                do {
                    let jsonObject = try JSONSerialization.jsonObject(with: d, options: [])
                    if let responseDict = jsonObject as? Dictionary<String, Any>, let infoData = responseDict["successResult"] as? [String: Any], let botData = infoData["bot"] as? [String:Any] {
                        
                        let botInfo = VTBotInfo(withDictionary: botData)
                        completion(botInfo, nil)
                    }
                    else {
                        completion(nil, NSError(domain: "NET_ERROR", code: 900, userInfo: nil))
                    }
                }
                catch {
                    completion(nil, NSError(domain: "NET_ERROR", code: 900, userInfo: nil))
                }
            }
            else {
                
            }
        }
    }
    
    open func getMenuItem(withBotID botID: String, completion : @escaping (_ menuItems : [VTMenuItem]?, _ error : NSError?) -> Void) {
        VTRestAPI.GET(withBaseURL: strBaseURL, withServiceNVersion: strPlatformVersion, withPath: "botinfo/menu/\(botID)", withParams: nil) { (data : Data?, _ error: NSError?) in
            if let d = data {
                
                do {
                    let jsonObject = try JSONSerialization.jsonObject(with: d, options: [])
                    
                    if let responseDict = jsonObject as? Dictionary<String, Any>, let menuData = responseDict["successResult"] as? [String: Any], let menuArray = menuData["ChatBotMenuItem"] as? [[String:Any]] {
                        
                        var menuItems : [VTMenuItem]? = nil
                        
                        for menu in menuArray {
                            if menuItems == nil {
                                menuItems = [VTMenuItem]()
                            }
                            let vtMenuItem = VTMenuItem(withDictionary : menu)
                            menuItems?.append(vtMenuItem)
                        }
                        
                        completion(menuItems, nil)
                    }
                    else {
                        completion(nil, NSError(domain: "NET_ERROR", code: 900, userInfo: nil))
                    }
                }
                catch {
                    completion(nil, NSError(domain: "NET_ERROR", code: 900, userInfo: nil))
                }
            }
            else {
                completion(nil, NSError(domain: "NET_ERROR", code: 900, userInfo: nil))
            }
        }
    }
    
    // MARK: - socket
    open func writeText(_ text : String) {
        print("user message json :\(text)")
        self.socket?.emit("usermessage", text)
    }
    
    open func isConnected()->Bool{
        if let socket = self.socket{
            if socket.status == .connected{
                return true;
            }
        }
        return false;
    }
    
    open func writeObject(_ obj : [String: Any]) {
        print("user message json :\(obj)")
        self.socket?.emit("usermessage", obj)
    }
}
