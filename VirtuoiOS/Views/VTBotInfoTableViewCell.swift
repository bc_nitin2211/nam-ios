//
//  VTBotInfoTableViewCell.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/17/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTBotInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(_ botInfo : VTBotInfo) {
        if let urlstr = botInfo.imageURLString, let url = Foundation.URL(string: urlstr) {
            thumbnailImageView.af_setImage(withURL: url)
        }
        titleLabel.text = botInfo.name
        descriptionLabel.text = botInfo.botDescription
    }
}
