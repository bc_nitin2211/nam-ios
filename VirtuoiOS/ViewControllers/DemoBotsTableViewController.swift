//
//  DemoBotsTableViewController.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/17/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class DemoBotsTableViewController: UITableViewController {
    
    var demoBots : [VTBotInfo]? = nil;

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Bot List"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        VTNetworkManager.manager.getBotList { (bots: [VTBotInfo]?, error: NSError?) in
            self.demoBots = bots
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        guard  let _ = demoBots else {
            return 0
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard  let bots = demoBots else {
            return 0
        }
        return bots.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "botListTableViewCell", for: indexPath) as! VTBotInfoTableViewCell

        let botInfo = demoBots![indexPath.row]
        cell.setData(botInfo)

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let cell = sender as? UITableViewCell, let indexPath = self.tableView.indexPath(for: cell) {
            let vc : ChatViewController = segue.destination as! ChatViewController
            let botInfo = demoBots![indexPath.row]
            VTUserDataManager.manager.selectedBotID = botInfo.botID
            vc.title = botInfo.name
            let backItem = UIBarButtonItem()
            backItem.title = " "
            navigationItem.backBarButtonItem = backItem
            vc.selectedBot = demoBots![indexPath.row]
        }
    }
    

}
