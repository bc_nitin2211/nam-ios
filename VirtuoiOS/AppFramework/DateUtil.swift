//
//  DateUtil.swift
//  Reach-Swift
//
//  Created by akash savediya on 14/08/17.
//  Copyright © 2017 Kartik. All rights reserved.
//

import UIKit

class DateUtil: NSObject {

    static let UI_TIME_FORMAT = "HH:mm";
    static let UI_DATE_FORMAT = "dd/MM/yyyy";
    static func convertTimeToDateString(timeInMillis : Int64, convertFormat : String)->String{
        let timeInSec = timeInMillis/1000;
        let date = Date.init(timeIntervalSince1970: TimeInterval(timeInSec));
        let dateFormatter = DateFormatter()
        //Set timezone that you want
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = convertFormat //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate;
    }
}
