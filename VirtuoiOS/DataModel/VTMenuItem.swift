//
//  VTMenuItem.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/1/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

open class VTMenuItem: NSObject {
    open let menuId : String
    open let botId : String
    open let type : String
    open let title : String?
    open let payload : String?
    open let ordinalNumber: Int?
    open var isReadOutOn = false;
    init(_ isReadOutOn : Bool) {
        self.menuId = ""
        self.botId = ""
        self.type = ""
        if isReadOutOn{
            self.title = "Read Out Off"
        }else{
            self.title = "Read Out Loud"
        }
        self.isReadOutOn = isReadOutOn;
        self.payload = ""
        self.ordinalNumber = 0;
        
    }
    
    init(withDictionary dict : [String:Any]) {
        self.menuId = dict["id"] as! String
        self.botId = dict["chatBotId"] as! String
        self.type = dict["type"] as! String
        self.title = dict["title"] as? String
        self.payload = dict["payload"] as? String
        self.ordinalNumber = dict["ordinalNumber"] as? Int
    }
    
    /*
     {"object":"page","entry":[{"id":"d5712d34-db4a-47c3-bc2b-7bc0c152de3f","time":1491711510006,"messaging":[{"sender":{"id":"b3f69a68-896d-4f7d-b8f4-8eda86a50572"},"recipient":{"id":"18a49990d1290b96930f38e0641f5999f53475aa"},"timestamp":1491711510006,"postback":{"payload":"help"}}]}]}
    */
    open func makeMessageObject() -> [String: Any] {
        let timeStamp = Date().timeIntervalSince1970
        let iTime = Int64(timeStamp*1000)
        let sTime = String(iTime)
        
        let postback = ["payload": payload!] as [String : Any]
        let sender = ["id" : VTUserDataManager.manager.userId]
        let recipient = ["id" : botId]
        
        let messaging = [["sender": sender , "recipient": recipient, "timestamp": sTime, "postback": postback, "accessTokens":VTBotInfo.getAccessTokensForNam()]] as [[String : Any]]
        let entry = [["id": menuId, "time": sTime, "messaging" : messaging]] as [[String : Any]]
        let usermessage = ["object": "page", "entry" : entry] as [String : Any]
        
        print("\(usermessage)")
        
        return usermessage
    }
    
}
