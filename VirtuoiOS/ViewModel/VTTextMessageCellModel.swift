//
//  VTTextMessageCellModel.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/5/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTTextMessageCellModel : VTCellModel {
    var textMessage : VTTextMessage!
    
    init(withTextMessage message: VTTextMessage) {
        textMessage = message
    }
    
    func isUsers() -> Bool {
        return textMessage.senderId == VTUserDataManager.manager.userId
    }
    
    func cellForModel(_ tableView : UITableView) -> UITableViewCell {
        let cell : VTChatTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VTChatTableViewCell") as! VTChatTableViewCell
        cell.textView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cell.textView.text = self.textMessage.text
        let size = cell.textView.sizeThatFits(CGSize(width: 240, height: CGFloat.greatestFiniteMagnitude))
        
        
        if (isUsers()) {
            cell.textView.textAlignment = .right
            
            cell.textView.frame = CGRect(x: tableView.frame.size.width - 30 - size.width, y: 10, width: size.width, height: size.height + 1)
            cell.textView.textColor = UIColor.white
            cell.textView.font = UIFont(name: "Roboto-Regular", size: 14)
            let image = UIImage(named: "rbubble")!.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 20), resizingMode: .stretch)
            cell.backgroundImageView.image = image
            cell.backgroundImageView.frame = CGRect(x: tableView.frame.size.width - 40 - cell.textView.frame.size.width, y: 5, width: size.width + 30, height: size.height+10)
            cell.isIncomingCell = false;
        }
        else {
            cell.textView.textAlignment = .left
            cell.isIncomingCell = true;
            cell.textView.frame = CGRect(x: 25, y: 10, width: size.width, height: size.height + 1)
            cell.textView.textColor = UIColor.init(hex: AppColors.CLR_BOT_MESSAGE_TEXT)
            cell.textView.font = UIFont(name: "Roboto-Regular", size: 14)
            let image = UIImage(named: "lbubble")!.resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 10), resizingMode: .stretch)
            
            cell.backgroundImageView.image = image
            
            cell.backgroundImageView.frame = CGRect(x: 10, y: 5, width: size.width + 30, height: size.height + 10)
            
        }
        return cell;
    }
    
    func cellHeight(_ tableView: UITableView) -> CGFloat {
        let str = textMessage.text
        
        let size = self.getSize(str!)
        
        return size.height + 10
    }
    
    func getSize(_ text: String) -> CGSize {
        let textView = UITextView()
        textView.font = UIFont(name: "Avenir-Medium", size: 14)!
        textView.text = text
        let size = textView.sizeThatFits(CGSize(width: 240, height: CGFloat.greatestFiniteMagnitude))
        
        return size;
    }
}
