//
//  ChatViewController.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/1/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import Speech

class ChatViewController: BaseVC , VTChatViewDelegate {

    var selectedBot : VTBotInfo!
    var chatView : VTChatView!
    var subtitle : String!
    var selectedAttachButtonData : VTAttachmentButtonData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "News America Marketing"
        self.automaticallyAdjustsScrollViewInsets = false;
//        self.selectedBot = VTBotInfo(withDictionary: ["id" : "099b4cdd4e634ba1731e053d5eac6d9ca174bd12",
//                                                      "name": "News America Marketing",
//                                                      "chatBotPlatformUserId": "5da7e3ab3530bce5ff44336fde8a73982dff4691",
//                                                      "description": "Welcome to the News America Marketing Salesforce contacts bot.",
//                                                      "status": "NEW"])
        
        //self.navigationController?.navigationItem.backBarButtonItem? = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        chatView = VTChatView.instanceFromNib()
        chatView.delegate = self
        chatView.selectedBot = self.selectedBot
        let top = UIApplication.shared.statusBarFrame.size.height + self.navigationController!.navigationBar.frame.size.height
        let rect = CGRect(x: 0, y: top, width: self.view.bounds.size.width, height: self.view.bounds.size.height - top)
        chatView.frame = rect
        
        self.view.addSubview(chatView)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notificaiton:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        chatView.connect()
        
        VTSpeechRecognizer.manager.getAuth { (status: SFSpeechRecognizerAuthorizationStatus) in
            self.chatView.trySpeakButton.isHidden = (status != .authorized)
        }
        
        self.navigationItem.setTitle(title : "News America Marketing", subtitle : subtitle)
        setRightMenuButton()
    }
    
    func setRightMenuButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_popup"), style: .plain, target: self, action: #selector(self.onMenuIconClicked))
    }
    
    func onMenuIconClicked(){
        showActionSheet(title: "News America Marketing", message: nil)
    }
    
    func signOutUser(){
        popOrDismissVC(nil, reqCode: REQ_CODE.SIGN_OUT)
    }
    
    func showActionSheet(title: String?,message : String?)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Sign Out", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            self.signOutUser()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationItem.backBarButtonItem?.title = "ABC"
        self.navigationItem.hidesBackButton = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func keyboardNotification(notification: NSNotification) {
//        if let userInfo = notification.userInfo {
//            if let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//                self.chatView.keyboardFrameChanged(endFrame)
//            }
//        }
//    }
    
    func keyboardWillShow(notification : NSNotification) {
        if let userInfo = notification.userInfo {
            if let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.chatView.keyboardShow(endFrame)
            }
        }
    }
    
    func keyboardWillHide(notificaiton : NSNotification) {
        self.chatView.keyboardHide()
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "WebViewSegue" {
            if let buttonData = sender as? VTAttachmentButtonData, let vc = segue.destination as? VTWebViewController {
                vc.webURL = buttonData.webURL
            }
        }
    }
 
    func gotoWebButtonClicked(_ buttonData: VTAttachmentButtonData) {
        selectedAttachButtonData = buttonData
        self.performSegue(withIdentifier: "WebViewSegue", sender: selectedAttachButtonData)
    }
    
}
