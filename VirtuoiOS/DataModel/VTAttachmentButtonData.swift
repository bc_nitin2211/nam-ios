//
//  VTAttachmentButtonData.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/12/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import Foundation

open class VTAttachmentButtonData {
    open var payload : String?
    open var buttonTitle : String?
    open var buttonType : String?
    open var webURL : String?
    
    class open func parseAttachmentButtonData(_ array : [[String: Any]]) -> [VTAttachmentButtonData] {
        var returnArray = [VTAttachmentButtonData]()
        
        for dict in array {
            let value = VTAttachmentButtonData(withDictionary: dict)
            returnArray.append(value)
        }
        
        return returnArray
    }
    
    init(withDictionary dict : [String : Any]) {
        payload = dict["payload"] as? String
        buttonType = dict["type"] as? String
        buttonTitle = dict["title"] as? String
        webURL = dict["url"] as? String
    }
    
    open func makeMessageObject(toBotID botID: String, withTextMessage textMessage : VTTextMessage) -> [String: Any] {
        let timeStamp = Date().timeIntervalSince1970
        let iTime = Int64(timeStamp*1000)
        let sTime = String(iTime)
        
        //let pay = self.payload?.replacingOccurrences(of: "<messageName>", with: "")
        let postback = ["payload": self.payload]
        let sender = ["id" : VTUserDataManager.manager.userId]
        let recipient = ["id" : botID]
        
        let messaging = [["sender": sender , "recipient": recipient, "timestamp": sTime, "postback": postback, "accessTokens":VTBotInfo.getAccessTokensForNam()]] as [[String : Any]]
        let entry = [["id": textMessage.messageId, "time": sTime, "messaging" : messaging]] as [[String : Any]]
        let usermessage = ["object": "page", "entry" : entry] as [String : Any]
        
        return usermessage
    }
}
