//
//  BaseVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class BaseVC: UIViewController, RestDelegate, VCCallback {
    
    var resultDelegate : VCCallback?
    var delegate : RestDelegate!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;
        if isInitNavigationBar(){
            initNavigationBar()
        }
        // Do any additional setup after loading the view.
    }
    
    func isInitNavigationBar()->Bool{
        return false;
    }
    
    func initNavigationBar(){
        setNavigationAppearance()
        title = getNavigationbarTitle()
        addNavigationButtons()
    }
    
    func setNavigationAppearance(){
        UINavigationBar.appearance().barTintColor = UIColor.init(hex: AppColors.PRIMARY_DARK)
        UINavigationBar.appearance().tintColor = UIColor.init(hex: AppColors.TINT_COLOR)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        //        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        //        let titleFont = FontUtil.getFont(fontName: FontUtil.MEDIUM, fontSize: 16.0)
        //        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: titleFont]
    }
    
    func addNavigationButtons(){
        
    }
    
    func getNavigationbarTitle()->String{
        return "";
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadData(httpRequest: HttpObject){
        let downloader = AlamofireManager(httpRequest: httpRequest, restDelegate: self);
        downloader.startDownload();
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // Mark: RestDelegate
    func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        print("OnPreExecute");
        startLoader()
    }
    
    func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        print("onSuccess");
        stopLoader()
        if let response = object.responseObject as? BaseResponse{
            return response.success!
        }
        return true;
    }
    
    func logOut(){
        DefaultsUtil.logoutUser();
        //        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! ViewController
        //        self.present(viewController, animated: true){}
    }
    
    func onFailure(_ paramObject: HttpResponse, forTaskCode taskCode: TASKCODES) {
        print("onFailure");
        stopLoader()
    }
    
    
    func stopLoader ()
    {
       
    }
    func startLoader ()
    {
        
    }
    func showalert (title: String,message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func pushVC(vc : UIViewController){
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Writeblogview") as! WriteBlogView
    //    self.navigationController?.pushViewController(viewController, animated: true)
    
    
    func popOrDismissVC(_ data : [String:Any]?, reqCode : REQ_CODE){
        if let delegate = resultDelegate{
            delegate.onVCResult(data, reqCode: reqCode, resCode: .SUCCESS)
        }
        if let navController = self.navigationController{
            navController.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil);
        }
    }
    
    func onVCResult(_ data: [String : Any]?, reqCode: REQ_CODE, resCode: RESULT_CODE) {
        
    }
}
