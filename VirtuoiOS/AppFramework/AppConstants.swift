//
//  AppConstants.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

enum METHODS : Int {
    case GET
    case POST
    case PUT
    case DELETE
    case PATCH
}

enum STATUS_TYPE{
    case EMAIL
    case PHONE
    case NONE
}

enum REQ_CODE : Int{
    case SAML_RESPONSE
    case DEFAULT
    case SIGN_OUT
}

enum RESULT_CODE : Int{
    case SUCCESS
    case FAILURE
}

enum HOME_CELL_TYPE : Int {
    case HEADER
}

enum DrawerIndex : Int {
    case HOME
}

enum CellAction : Int {
    case COMMENT_ARROW_CLICK
}

enum NotificationActions : Int{
    case BOOK_APPOINTMENT = 1
}

enum TASKCODES : Int{
    case GET_NAM_CONFIG
    case GET_ACCESS_TOKEN
    case GET_USER_INFO
}
enum HttpResponseResult : Int{
    case SUCCESS
    case FAILURE
}

enum COLORS : String{
    case PRIMARY = "#1781be"
    case PRIMARY_DARK = "#2196F5"
    case TITLE_COLOR = "#ffffff"
    case SUBTITLE_COLOR = "#465783"
    case HIGHLITED_TEXTCOLOR = "#2196F3"
    case TEXT_BLUE = "#2196F6"
}

struct STRINGS{
    static let EMPTY_SUBJECT = "Field can not be empty";
    static let INVALID_RESPONSE = "Oops some unknown error occurred";
}

struct AppColors{
    static let PRIMARY = "#1781be";
    static let PRIMARY_DARK = "#1781be";
    static let NAV_TITLE = "#ffffff";
    static let TINT_COLOR = "#ffffff";
    static let CLR_BOT_MESSAGE_TEXT = "#000000";
}

struct URL {
    static let BASE_URL = "https://platformservice.botcentralapi.com/";
    static let USER_INFO = "\(BASE_URL)salesforce-crm-0.1/user/fetchInfo";
//    static let USER_INFO = "http://192.168.1.12:8080/salesforce-crm-0.1/user/fetchInfo";
    static let NAM_CONFIG = "\(BASE_URL)bot-platform-manager-0.1/enterpriseIntegrations";
    
}

struct PARAMS {
    static let TOKEN = "5unvdSlNzmKSf79oq0wjK1i79gYWNoVr5EWMPGLvDSAS7gzoalHuueMc2+DmpX2yRok/jEveyMQ=";
    static let CHAT_BOT_ID : String = "099b4cdd4e634ba1731e053d5eac6d9ca174bd12"
    static let BOT_NAME : String = "News America Marketing"
    static let BOT_START_PAYLOAD : String = "hi"

    static let GRANT_TYPE = "assertion";
    static let ASSERTION_TYPE = "urn:oasis:names:tc:SAML:2.0:profiles:SSO:browser";
    
//    static let OKTA_LOGIN_URL = "https://botcentral.okta.com/";
    static let OKTA_LOGIN_URL = "https://botcentral.okta.com/app/botcentral_oktasamltestsetup_1/exk10mj5pb3FcY0OM2p6/sso/saml"
    static let SALESFORCE_LOGIN = "https://botcentral.okta.com/app/botcentral_oktasamltestsetup_1/exk10mj5pb3FcY0OM2p6/sso/saml";
//    static let OAUTH_END_POINT = "https://botcentral.my.salesforce.com/services/oauth2/token?so=00D7F000001ZA2s";
    static let OAUTH_END_POINT = "https://nam--UAT.cs19.my.salesforce.com/services/oauth2/token?so=00D290000000XuL"
    
    static let map : [String : String] = ["one": "1","two":"2","three":"3",
                                          "four":"4", "five":"5", "six":"6",
                                          "seven":"7", "eight":"8", "nine":"9",
                                          "zero":"0", "underscore":"_", "under score":"_",
                                          "hyphen":"-", "hyphan":"-", "minus":"-",
                                          "dash":"-", "at":"@", "dot":"."]
}

protocol RestDelegate: NSObjectProtocol {
    func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool
    func onFailure(_ paramObject: HttpResponse, forTaskCode taskCode: TASKCODES)
    func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES)
}

protocol DrawerItemClickListener{
    func onDrawerItemClicked(index : DrawerIndex)
}

protocol VCCallback{
    func onVCResult(_ data : [String : Any]?, reqCode : REQ_CODE, resCode : RESULT_CODE)
}
protocol BaseHomeCell {
    func getCellType()->HOME_CELL_TYPE
}

protocol CellActionListner {
    func onCellAction(actionType : CellAction, position : Int);
}
