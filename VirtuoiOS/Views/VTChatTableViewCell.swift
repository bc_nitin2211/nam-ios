//
//  VTChatTableViewCell.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/3/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

class VTChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var textView : UITextView!
    var tapGesture : UITapGestureRecognizer?
    var isIncomingCell = false;
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(onTextClicked))
        textView.addGestureRecognizer(tapGesture!);
    }
    
    func onTextClicked(){
        if isIncomingCell{
            if let text = self.textView.text{
                T2SConverter.getInstance().speakText(text);
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
