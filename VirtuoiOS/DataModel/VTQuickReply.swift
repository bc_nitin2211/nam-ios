//
//  VTQuickReply.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/5/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import Foundation

open class VTQuickReply {
    open var contentType : String!
    open var payload : String!
    open var title : String!
    open var seq : Int!
    open var delay : Int!
    
    open var recipientId : String!
    open var messageId : String!
    
    init(withDictionary dict : [String: Any], withRecipientId recipientId : String, withSeq seq: Int?, withDelay delay: Int?) {
        contentType = dict["content_type"] as! String
        payload = dict["payload"] as! String
        
        title = dict["title"] as! String
        
        self.seq = seq ?? 0
        self.delay = delay ?? 0
        self.recipientId = recipientId
        self.messageId = UUID().uuidString
    }
    
    open func makeMessageObject() -> [String: Any] {
        let timeStamp = Date().timeIntervalSince1970
        let iTime = Int64(timeStamp*1000)
        let sTime = String(iTime)
        
        let quickReply = ["payload": self.payload]
        let message = ["mid": "mid." + sTime + ":" + messageId, "text": self.title, "seq": seq, "delay": delay, "quick_reply": quickReply] as [String : Any]
        let sender = ["id" : VTUserDataManager.manager.userId]
        let recipient = ["id" : recipientId]
        
        let messaging = [["sender": sender , "recipient": recipient, "timestamp": sTime, "message": message, "accessTokens":VTBotInfo.getAccessTokensForNam()]] as [[String : Any]]
        let entry = [["id": messageId, "time": sTime, "messaging" : messaging]] as [[String : Any]]
        let usermessage = ["object": "page", "entry" : entry] as [String : Any]
        
        print("\(usermessage)")
        
        return usermessage
    }
}
