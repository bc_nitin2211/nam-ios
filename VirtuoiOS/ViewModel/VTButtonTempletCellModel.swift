//
//  VTButtonTempletCellModel.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/19/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

protocol VTButtonTempletCellModelDelegate : class {
    func touchAttachmentButton(_ attachment : VTAttachmentButtonData)
}

class VTButtonTempletCellModel: VTCellModel {
    var text : String?
    var buttonData : [VTAttachmentButtonData]!
    
    weak var delegate : VTButtonTempletCellModelDelegate?
    
    init(withDictionary dict : [String: Any]) {
        text = dict["text"] as? String
        buttonData = VTAttachmentButtonData.parseAttachmentButtonData(dict["buttons"]! as! [[String : Any]])
    }
    
    func cellHeight(_ tableView: UITableView) -> CGFloat {
        let rect = text!.boundingRect(with: CGSize(width: tableView.frame.width - 77 - 16, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 14)!], context: nil)
        
        return rect.height + CGFloat(buttonData.count * 35) + 20.0
    }
    
    func cellForModel(_ tableView: UITableView) -> UITableViewCell {
        let cell : VTButtonTempleteTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VTButtonTempleteTableViewCell") as! VTButtonTempleteTableViewCell
        cell.titleLabel.text = text;
        let rect = text!.boundingRect(with: CGSize(width: tableView.frame.width - 77 - 16, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont(name: "Roboto-Medium", size: 14)!], context: nil)
        
        cell.bgView.layer.cornerRadius = 5.0
        cell.bgView.clipsToBounds = true
        
        for subView in cell.stackView.subviews {
            cell.stackView.removeArrangedSubview(subView)
            subView.removeFromSuperview()
        }
        
        for i in 0 ..< self.buttonData.count {
            let data = self.buttonData[i]
            
            let btn = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width-63, height: 35))
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            btn.setTitleColor(UIColor(colorLiteralRed: 74.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0), for: .normal)
            btn.setTitle(data.buttonTitle, for: .normal)
            btn.backgroundColor = UIColor.white
            
            let line = UIView(frame: CGRect(x: 0, y: 0, width: btn.frame.width + 10, height: 0.5))
            line.backgroundColor = UIColor.lightGray
            btn.addSubview(line)
            btn.clipsToBounds = true
            
            btn.tag = i
            
            btn.addTarget(self, action: #selector(self.attachmentButtonClicked(_:)), for: .touchUpInside)
            
            cell.stackView.addArrangedSubview(btn)
        }
        
        cell.txtBgView.update(x: nil, y: nil, width: nil, height: rect.height + 4)
        
        return cell;
    }
    
    @IBAction func attachmentButtonClicked(_ button : UIButton) {
        let attachment = self.buttonData[button.tag]
        
        self.delegate?.touchAttachmentButton(attachment)
    }
}
