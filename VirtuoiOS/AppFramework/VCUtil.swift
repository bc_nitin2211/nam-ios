//
//  VCUtil.swift
//  BrewBound
//
//  Created by Nitin Bansal on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
class VCUtil: NSObject {
    
    static func getStoryBoard(name : String)->UIStoryboard{
        return UIStoryboard(name: name, bundle: Bundle.main)
    }
    
    static func getViewController(storyBoard: UIStoryboard, identifier : String)->UIViewController{
        return storyBoard.instantiateViewController(withIdentifier: identifier)
    }
    
    static func getViewController(identifier : String)->UIViewController{
        return getStoryBoard(name: "Main").instantiateViewController(withIdentifier: identifier)
    }
    
    //    static func getDrawerVC(nibName : String)->DrawerVC{
    //        return DrawerVC(nibName: "DrawerVC", bundle: nil);
    //    }
    
    static func setShadowView(shadowView: UIView, cornerRadius : CGFloat){
        shadowView.layer.cornerRadius = cornerRadius;
        shadowView.layer.borderColor = UIColor.lightGray.cgColor;
        shadowView.layer.borderWidth = 0.4;
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor;
        shadowView.layer.shadowOpacity = 1.0
        shadowView.layer.shadowRadius = 0.2
        shadowView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
    }
    
    static func roundUILabel(label: UILabel){
        label.layer.cornerRadius = label.frame.width/2;
        label.clipsToBounds = true;
    }
    
    static func setHtmlText(label : UILabel, htmlText : String){
        if let htmlData = htmlText.data(using: String.Encoding.unicode) {
            do {
                let attributedText = try NSAttributedString(data: htmlData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                label.attributedText = attributedText;
            } catch let e as NSError {
                print("Couldn't translate \(htmlText): \(e.localizedDescription) ")
            }
        }
    }
    
    static func getRelativeTimeString(serverTimeInMillis : Int64)->String{
        let time = NSDate(timeIntervalSince1970: TimeInterval(serverTimeInMillis/1000)).timeIntervalSinceNow
        let relativeTime = NSDate.relativeTimeInString(value: time)
        return relativeTime;
    }
    
    static func setAttributedString(completeText : String, textToAttribute : String, label : UILabel){
        let range = (completeText as NSString).range(of: textToAttribute);
        let attributedString = NSMutableAttributedString(string:completeText)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(hex: COLORS.TEXT_BLUE.rawValue) , range: range)
        label.attributedText = attributedString;
    }
    static func setAttributedString(completeText : String, words : [String], label : UILabel){
        
        let attributedString = NSMutableAttributedString(string:completeText)
        for word in words{
            let range = (completeText as NSString).range(of: word);
            attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.init(hex: COLORS.TEXT_BLUE.rawValue) , range: range)
        }
        
        label.attributedText = attributedString;
    }
    
    
    
    static func getChatMenuItemButton(_ title : String, i : Int, width : CGFloat)->UIButton{
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: 28))
        btn.setTitle(title, for: .normal)
        btn.tag = i
        btn.setTitleColor(UIColor.init(hex: AppColors.CLR_BOT_MESSAGE_TEXT), for: .normal)
        btn.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)
        btn.contentHorizontalAlignment = .left
        btn.contentVerticalAlignment = .center
        btn.contentEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
        return btn;
    }
    
    static func getChatMenuItemLine(width : CGFloat)->UIView{
        let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 1))
        view.backgroundColor = UIColor.black
        return view;
    }
    
    
    
    
}
