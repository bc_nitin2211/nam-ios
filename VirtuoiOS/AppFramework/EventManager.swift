//
//  EventManager.swift
//  Reach-Swift
//
//  Created by Nitin Bansal on 15/08/17.
//  Copyright © 2017 Kartik. All rights reserved.
//

import UIKit

class EventManager: NSObject {
    public static let REFRESH_ACTION = "refreshAction"
    var listenerSet = [String : Set<AnyHashable>]();
    private static var sharedInstance = EventManager();
    static func getInstance()->EventManager{
        return sharedInstance
    }
    func add<T>(eventName : String, delegate : T) where T : RefreshListener, T : Hashable{
        print("Add Listener")
        if var set = listenerSet[eventName]{
            set.insert(delegate)
            listenerSet.updateValue(set, forKey: eventName)
        }else{
            var set = Set<AnyHashable>();
            set.insert(delegate)
            listenerSet.updateValue(set, forKey: eventName)
        }
        
    }
    
    func remove<T>(eventName : String, delegate : T) where T : RefreshListener, T : Hashable{
        print("Remove Listener")
        if var set = listenerSet[eventName]{
            set.remove(delegate)
            listenerSet.updateValue(set, forKey: eventName)
        }else{
        }
    }
    
    
    func sendBroadcast(eventName : String){
        if let set = listenerSet[eventName]{
            for item in set{
                let delegate = item as? RefreshListener
                print("Send Bcast")
                delegate?.onEventAction(eventName: eventName);
            }
        }
    }
    
    
}
protocol RefreshListener {
    func onEventAction(eventName : String)
}
