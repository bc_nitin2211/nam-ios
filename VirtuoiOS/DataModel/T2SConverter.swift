//
//  T2SConverter.swift
//  VirtuoiOS
//
//  Created by Nitin Bansal on 05/09/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import AVFoundation
class T2SConverter: NSObject, AVSpeechSynthesizerDelegate {

    private var avSpeaker = AVSpeechSynthesizer()
    static private var converter = T2SConverter();
    
    static func getInstance()->T2SConverter{
        return converter;
    }
    
    func speakText(_ text : String){
        if !VTBotInfo.getRunningInstance().isReadOutOn{
            return;
        }
        if avSpeaker.isSpeaking{
            avSpeaker.stopSpeaking(at: .immediate);
        }
        try! AVAudioSession.sharedInstance().setActive(true)
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        print("Volume = \(AVAudioSession.sharedInstance().outputVolume)")
        avSpeaker.delegate = self;
        let voice = AVSpeechUtterance.init(string: text);
        avSpeaker.speak(voice);
    }
}
