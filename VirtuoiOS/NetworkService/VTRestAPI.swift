//
//  VTRestAPI.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 2/28/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit
import Alamofire

class VTRestAPI: NSObject {
    class open func GET(withBaseURL baseURL : String, withServiceNVersion serviceVersion: String, withPath path: String?, withParams params : [String: Any]?, withCompletion completion: @escaping (_ data: Data?, _ error: NSError?) -> Void ) {
        let urlString = VTRestAPI.makeURL(baseURL, withVersion: serviceVersion, withPath: path)
        
        let _ = Alamofire.request(urlString, method: .get, parameters: params, encoding : URLEncoding.default, headers: nil).responseData(completionHandler: { (response : DataResponse<Data>) in
            completion(response.data, response.error as NSError?)
        })
    }
    
    class open func PUT(withBaseURL baseURL : String, withServiceNVersion serviceVersion: String, withPath path: String?, withParams params : [String : Any]?, withCompletion completion: @escaping (_ data: Data?, _ error: NSError?) -> Void ) {
        let urlString = VTRestAPI.makeURL(baseURL, withVersion: serviceVersion, withPath: path)
        
        let _ = Alamofire.request(urlString, method: .put, parameters: params, encoding : URLEncoding.default, headers: nil).responseData(completionHandler: { (response : DataResponse<Data>) in
            completion(response.data, response.error as NSError?)
        })
    }
    
    class open func makeURL(_ baseURL : String , withVersion version: String, withPath path : String?) -> String {
        var url = baseURL
        url += "/\(version)"
        if let p = path {
            url += "/\(p)"
        }
        
        return url
    }
}
