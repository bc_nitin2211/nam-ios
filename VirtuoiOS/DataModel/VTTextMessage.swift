//
//  VTMessage.swift
//  VirtuoiOS
//
//  Created by Wontai Ki on 3/4/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import Foundation

open class VTTextMessage {
    open var timeStamp : TimeInterval!
    open var text : String!
    open var messageId : String!
    open var senderId : String!
    open var recipientId : String!
    
    init(withDictionary dict: [String : Any]) {
        
    }
    
    init(withText text: String, fromSenderId senderId : String, toRecipientId recipientId : String) {
        timeStamp = Date().timeIntervalSince1970
        self.text = text
        self.senderId = senderId
        self.recipientId = recipientId;
        self.messageId = UUID().uuidString
    }
    
    open func makeMessageObject() -> [String: Any] {
        timeStamp = Date().timeIntervalSince1970
        let iTime = Int64(timeStamp*1000)
        let sTime = String(iTime)
        
        let message = ["mid": "mid." + sTime + ":" + messageId, "text": self.text]
        let sender = ["id" : senderId]
        let recipient = ["id" : recipientId]
        
        let messaging = [["sender": sender , "recipient": recipient, "timestamp": sTime, "message": message, "accessTokens":VTBotInfo.getAccessTokensForNam()]] as [[String : Any]]
        let entry = [["id": messageId, "time": sTime, "messaging" : messaging]] as [[String : Any]]
        let usermessage = ["object": "page", "entry" : entry] as [String : Any]
        
        return usermessage
    }
    open func makeMessageObject(_ botInfo : VTBotInfo) -> [String: Any] {
        timeStamp = Date().timeIntervalSince1970
        let iTime = Int64(timeStamp*1000)
        let sTime = String(iTime)
        
        let message = ["mid": "mid." + sTime + ":" + messageId, "text": self.text]
        let sender = ["id" : senderId]
        let recipient = ["id" : recipientId]
        
        let messaging = [["sender": sender , "recipient": recipient, "timestamp": sTime, "message": message, "accessTokens":botInfo.getAccessTokensArray()]] as [[String : Any]]
        let entry = [["id": messageId, "time": sTime, "messaging" : messaging]] as [[String : Any]]
        let usermessage = ["object": "page", "entry" : entry] as [String : Any]
        
        return usermessage
    }
}
