//
//  VTSpeechRecognizer.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 6/7/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import Foundation
import Speech
import SwiftySound

protocol VTSpeechRecognizerDelegate: class {
    func recognizeText(_ text: String, fullText : String, isFinal: Bool)
    func onSpeechListener(isStarted : Bool)
}

open class VTSpeechRecognizer : NSObject, SFSpeechRecognizerDelegate  {
    static open let manager: VTSpeechRecognizer = VTSpeechRecognizer()
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    weak var delegate: VTSpeechRecognizerDelegate?
    open var prevResult = "";
    
    open var timer = Timer();
    let maxInactivityTime = 5;
    var seconds = 5;
    var isTimerRunning = false;
    
    var currentTrascriptionIndex = 0;
    
    override init() {
        super.init()
        speechRecognizer.delegate = self
    }
    
    func onTimerTick(){
        print("onTimerTick")
        if seconds < 1{
            self.stop();
        }else{
            seconds -= 1;
        }
    }
    
    
    
    func initTimer(){
        print("initTimer")
        seconds = maxInactivityTime;
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.onTimerTick), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    open func start() {
        playStartSound()
    }
    
    open func startRec(){
        if !audioEngine.isRunning {
            print("Audio not Running, start recording")
            try! startRecording()
        }
    }
    
    open func startAfterSound(){
        if !audioEngine.isRunning {
            print("Audio not Running, start recording")
            try! startRecording()
        }else{
            print("Audio Running, stop recording")
            stop();
            startRec()
        }
    }
    
    open func playStartSound(){
        Sound.enabled = true;
        Sound.category = SoundCategory.ambient
        Sound.play(file: "listening.wav");
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.startAfterSound();
        })
    }
    
    open func playStopSound(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            Sound.enabled = true;
            Sound.category = SoundCategory.ambient
            Sound.play(file: "finish.wav")
        })
        
    }
    
    open func stop() {
        stopWithTimer(true)
    }
    
    open func stopWithTimer(_ stopTimer : Bool){
        print("Stop")
        if audioEngine.isRunning {
            print("AudioEngineRunning, Trying to stop")
            audioEngine.inputNode?.removeTap(onBus: 0)
            audioEngine.stop()
            recognitionTask?.cancel()
            recognitionRequest?.endAudio()
            self.prevResult = "";
        }
        if stopTimer{
            timer.invalidate()
            seconds = 0;
        }
        playStopSound()
    }
    
    open func cancel() {
        self.audioEngine.reset()
    }
    
    open func reset() {
        //        start()
    }
    
    open func resetAudio(){
        self.audioEngine.reset()
    }
    
    open func getAuth(withCompletion completion: @escaping (_ status: SFSpeechRecognizerAuthorizationStatus) -> Void) {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            completion(authStatus)
        }
    }
    
    open func authStatus() -> SFSpeechRecognizerAuthorizationStatus {
        return SFSpeechRecognizer.authorizationStatus()
    }
    
    open func startRecording() throws {
        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
        try audioSession.setMode(AVAudioSessionModeDefault)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode else { fatalError("Audio engine has no input node") }
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        
        // Configure request so that results are returned before audio recording is finished
        recognitionRequest.shouldReportPartialResults = true
        
        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false
            print("Speech manager : Recognition Task")
            if let result = result {
                var  text = result.bestTranscription.formattedString
                isFinal = result.isFinal
                
                print("Speech manager : Result Found \(text)")
                text = self.getResultedString(result.bestTranscription)
                let final = (error != nil || isFinal)
                if let d = self.delegate {
                    print("IsFinal \(result.isFinal)")
                    d.recognizeText(text, fullText : result.bestTranscription.formattedString, isFinal: final)
                    self.seconds = self.maxInactivityTime;
                }
            }
            print("Speech manager : Is Final : \(isFinal)")
            print("Speech manager : Error --> \(error != nil)")
            if error != nil || isFinal {
                self.stop()
                if let d = self.delegate {
                    d.onSpeechListener(isStarted: false)
                }
            }
            if error != nil{
                print("Error in speech")
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        if let d = self.delegate {
            d.onSpeechListener(isStarted: true)
        }
        initTimer()
        currentTrascriptionIndex = 0;
        try audioEngine.start()
    }
    
    func getResultedString(_ t : SFTranscription)->String{
        var string = "";
        if currentTrascriptionIndex < t.segments.count{
            for i in currentTrascriptionIndex ..< t.segments.count{
                let seg = t.segments[i];
                string.append(seg.substring)
                string.append(" ");
            }
            currentTrascriptionIndex = t.segments.count;
        }
        
        return string;
        
    }
    
    // MARK: SFSpeechRecognizerDelegate
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            
        } else {
            
        }
    }
    
    static func getConvertedSpeechText(_ text : String)->String{
        var lowercaseText = text.lowercased();
        var isReplaced = false;
        
        for (key, value) in PARAMS.map{
            let k = " " + key + " ";
            if lowercaseText.contains(k){
                isReplaced = true;
                lowercaseText = lowercaseText.replacingOccurrences(of: (" " + key), with: value)
            }
        }
        if (isReplaced) {
            lowercaseText = lowercaseText.condensedWhitespace
        }
        lowercaseText = lowercaseText.replacingOccurrences(of: " ", with: "")
        print("isReplaced \(isReplaced)")
        lowercaseText = lowercaseText.trimmingCharacters(in: .whitespacesAndNewlines)
        return lowercaseText;
    }
}

extension String {
    var condensedWhitespace: String {
        let components = self.components(separatedBy: .whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
}
