//
//  UIView+Virtuo.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/14/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

extension UIView {
    open var y : CGFloat {
        get {
            return self.frame.origin.y
        }
    }
    
    open var bottom : CGFloat {
        get {
            return (self.frame.origin.y + self.frame.size.height)
        }
    }
    
    open var x : CGFloat {
        get {
            return self.frame.origin.x
        }
        
    }
    
    open var right : CGFloat {
        get {
            return (self.frame.origin.x + self.frame.size.width)
        }
    }
    
    open func update(x : CGFloat?, y : CGFloat?, width : CGFloat?, height : CGFloat?) {
        let nx = x ?? self.x
        let ny = y ?? self.y
        let nw = width ?? self.frame.width
        let nh = height ?? self.frame.height
        
        self.frame = CGRect(x: nx, y: ny, width: nw, height: nh)
    }
    
}
