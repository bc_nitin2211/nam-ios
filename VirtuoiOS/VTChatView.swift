//
//  VTChatView.swift
//  VirtuoiOS
//
//  Created by Won Tai Ki on 3/1/17.
//  Copyright © 2017 Wontai Ki. All rights reserved.
//

import UIKit

protocol VTChatViewDelegate : class {
    func gotoWebButtonClicked(_ buttonData : VTAttachmentButtonData)
}

open class VTChatView: UIView, UITableViewDelegate, UITableViewDataSource, VTNetworkManagerDelegate, UITextFieldDelegate, VTAttachmentCellModelDelegate, VTButtonTempletCellModelDelegate, VTSpeechRecognizerDelegate {
    
    @IBOutlet weak var menuHeightContraint: NSLayoutConstraint!
    open var selectedBot : VTBotInfo!
    
    weak var delegate : VTChatViewDelegate?
    
    @IBOutlet var tableView : UITableView!
    @IBOutlet var inputTextField : UITextField!
    @IBOutlet var sendButton : UIButton!
    
    @IBOutlet var bottomStackView : UIStackView!
    @IBOutlet var textFieldAreaStackView : UIStackView!
    @IBOutlet var quickReplyScrollView : UIScrollView!
    
    @IBOutlet var speakView: UIView!
    @IBOutlet var speakLabel: UILabel!
    @IBOutlet var trySpeakButton: UIButton!
    @IBOutlet var speakButton: UIButton!
    @IBOutlet var clearButton: UIButton!
    @IBOutlet var speakCloseButton: UIButton!
    
    // Last vertical stackview
    @IBOutlet var menuOptionStackView : UIStackView!
    
    @IBOutlet var tapCatchView : UIView!
    
    @IBOutlet var panGesture : UIPanGestureRecognizer!
    
    //    @IBOutlet var grayBGView : UIView!
    //    @IBOutlet var whiteBGView : UIView!
    @IBOutlet var spacingView : UIView!
    
    open var panY : CGFloat = 0;
    open var bottomStackViewY : CGFloat = 0
    var statusType = STATUS_TYPE.NONE
    
    open var botInfo : VTBotInfo?
    var quickReplyItems : [VTQuickReply]! = [VTQuickReply]()
    open var quickReplyButtons : [UIButton]! = [UIButton]()
    
    open var cellModels : [VTCellModel]! = [VTCellModel]()
    open var typingCellModel : VTTypingCellModel = VTTypingCellModel()
    
    open var isKeyboard : Bool = false
    open var isSentHi : Bool = false;
    
    open var menuItems : [VTMenuItem]?
    
    open var textForVoice: String?
    
    open var lastOffsetDY: CGFloat = 0.0
    
    open var isSpeechLayoutOpen = false;
    
    open var isSpeechOn = false;
    
    let selector = #selector(actionPan(_:))
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    class func instanceFromNib() -> VTChatView {
        let chatView = UINib(nibName: "VTChatView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! VTChatView
        
        chatView.tableView.register(UINib(nibName: "VTChatTableViewCell", bundle: nil), forCellReuseIdentifier: "VTChatTableViewCell")
        chatView.tableView.register(UINib(nibName: "VTOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "VTOptionTableViewCell")
        chatView.tableView.register(UINib(nibName: "VTWaitingTableViewCell", bundle: nil), forCellReuseIdentifier: "VTWaitingTableViewCell")
        chatView.tableView.register(UINib(nibName: "VTButtonTempleteTableViewCell", bundle: nil), forCellReuseIdentifier: "VTButtonTempleteTableViewCell")
        
        chatView.panGesture.addTarget(chatView, action: chatView.selector)
        chatView.updateViewOffset(0)
        VTSpeechRecognizer.manager.delegate = chatView
        
        //        chatView.updateSpeakButton()
        
        return chatView;
    }
    
    
    
    // MARK: -
    func connect() {
        VTNetworkManager.manager.delegate = self;
        
        VTSpinner.manager.start()
        
        VTNetworkManager.manager.connectSocket(withURL: nil, /*withSId: sid,*/ withCompletion: { (error : NSError?) in
            if let e = error , e.code == 900 {
                VTSpinner.manager.stop()
            }
        })
        
        VTNetworkManager.manager.getMenuItem(withBotID: self.selectedBot.botID) { (menuItems: [VTMenuItem]?, error: NSError?) in
            self.menuItems = menuItems
            self.makeMenuItem()
        }
    }
    
    // MARK: - VTNetworkManagerDelegate
    
    func websocketDidConnect(_ manager: VTNetworkManager) {
        if self.isSentHi == false {
            self.isSentHi = true;
            self.perform(#selector(sendHi), with: nil, afterDelay: 0.0)
        }
        else {
            VTSpinner.manager.stop()
        }
    }
    
    func websocketDidReceive(_ data: [Any]) {
        for aData in data {
            if let dict = aData as? [String: Any] {
                parseMessage(dict)
            }
        }
        VTSpinner.manager.stop()
    }
    
    
    
    
    
    func sendHi() {
        let model = makeUserMessageCellModel("Hi")
        //self.cellModels.append(model)
        sendSocketMessage(model.textMessage.makeMessageObject())
    }
    
    func sendSocketMessage(_ msg : [String : Any]){
        self.quickReplyItems?.removeAll()
        if VTNetworkManager.manager.isConnected(){
            VTNetworkManager.manager.writeObject(msg)
            statusType = .NONE;
        }else{
            self.connect()
        }
    }
    
    @IBAction func replyButtonClicked(_ sender : UIButton) {
        self.inputTextField.resignFirstResponder()
        let quickReply = self.quickReplyItems![sender.tag]
        let model = makeUserMessageCellModel(quickReply.title)
        sendSocketMessage(quickReply.makeMessageObject())
        
        self.cellModels.append(model)
        
        // Clear buttons
        
        DispatchQueue.main.async {
            self.updateQuickReplyButtons()
            self.tableView.reloadData()
            self.updateViewOffset(0.0)
            self.goToLastCell()
        }
    }
    
    @IBAction func sendButtonClicked(_ sender: UIButton) {
        self.inputTextField.resignFirstResponder()
        if let txt = self.inputTextField.text, txt.characters.count > 0 {
            let model = makeUserMessageCellModel(txt)
            
            sendSocketMessage(model.textMessage.makeMessageObject())
            self.cellModels.append(model)
            
            DispatchQueue.main.async {
                self.speakClearButtonClicked(button: self.clearButton)
                self.updateQuickReplyButtons()
                self.updateViewOffset(0)
                self.tableView.reloadData()
                self.closeSpeechLayoutIfOpen()
                self.goToLastCell()
            }
        }
    }
    
    func touchAttachmentOptionButton(_ attachment: VTAttachmentItem, index : Int) {
        self.inputTextField.resignFirstResponder()
        if let attachDataArray = attachment.attachButtonDataArray, attachDataArray.count > 0 {
            let attachButton = attachDataArray[index]
            if attachButton.buttonType == "web_url" {
                delegate?.gotoWebButtonClicked(attachButton)
            }
            else {
                let model = makeUserMessageCellModel(attachButton.buttonTitle!)
                sendSocketMessage(attachButton.makeMessageObject(toBotID: selectedBot.botID, withTextMessage: model.textMessage))
                self.cellModels.append(model)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.goToLastCell()
                }
            }
        }
    }
    
    func touchAttachmentButton(_ attachButton: VTAttachmentButtonData) {
        self.inputTextField.resignFirstResponder()
        if attachButton.buttonType == "web_url" {
            delegate?.gotoWebButtonClicked(attachButton)
        }
        else {
            let model = makeUserMessageCellModel(attachButton.buttonTitle!)
            sendSocketMessage(attachButton.makeMessageObject(toBotID: selectedBot.botID, withTextMessage: model.textMessage))
            self.cellModels.append(model)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.goToLastCell()
            }
        }
    }
    
    func makeUserMessageCellModel(_ text: String) -> VTTextMessageCellModel {
        let m = VTTextMessage(withText: text, fromSenderId: VTUserDataManager.manager.userId, toRecipientId: selectedBot.botID)
        let cellModel = VTTextMessageCellModel(withTextMessage: m)
        return cellModel
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.sendButtonClicked(self.sendButton)
        self.textFieldValueChanged(textField)
        return true
    }
    
    @IBAction func textFieldValueChanged(_ textField : UITextField) {
        if let txt = textField.text , txt.characters.count > 0 {
            self.sendButton.isEnabled = true;
            
            self.textForVoice = txt.trimmingCharacters(in: .whitespacesAndNewlines);
        }
        else {
            self.sendButton.isEnabled = false;
        }
    }
    
    func goToLastCell() {
        guard tableView.numberOfSections > 0 , tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1) > 0 else {
            return
        }
        
        let indexPath = IndexPath(row: self.tableView.numberOfRows(inSection: self.tableView.numberOfSections - 1) - 1, section:self.tableView.numberOfSections - 1)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func actionPan(_ pan : UIPanGestureRecognizer) {
        if isKeyboard {
            return;
        }
        if (pan.state == .began) {
            self.panY =  pan.translation(in: self).y
            print("Start: \(self.panY)")
            bottomStackViewY = self.bottomStackView.frame.origin.y
        }
        else if (pan.state == .changed) {
            let cy = pan.translation(in: self).y
            
            let dy = cy - self.panY;
            print("DY: \(dy)")
            moveY(dy: dy)
            //self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: originalHeight + dy)
        }
        else /*if (pg.state == UIGestureRecognizerStateEnded)*/ {
            
            let cy = pan.translation(in:self).y;
            
            let dy = cy - self.panY;
            print("End DY: \(dy)")
            endY(dy: dy)
            //self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: originalHeight + dy)
            
        }
    }
    
    func updateViewOffset(_ dy: CGFloat) {
        print("updateViewOffset: \(dy)")
        self.lastOffsetDY = dy
        printHeights()
        //UIView.animate(withDuration: 0.5, animations: {
        self.bottomStackView.frame = CGRect(x: 0, y: self.frame.height - self.bottomStackView.frame.height - dy, width: self.bottomStackView.frame.size.width, height: self.bottomStackView.frame.size.height)
        self.tableView.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.bottomStackView.frame.origin.y)
        let p = self.spacingView.convert(CGPoint(x:0, y: 0), to: self)
        print("Speak View H:\(self.speakView.frame.height)");
        print("Space View Y:\(p.y)");
        print("Space View H:\(self.spacingView.frame.height)");
        self.bottomStackViewY = self.bottomStackView.frame.origin.y
        self.menuOptionStackView.frame = CGRect(x: 0, y: self.bottomStackView.frame.origin.y + self.bottomStackView.frame.height, width: self.menuOptionStackView.frame.width, height: self.menuOptionStackView.frame.height)
        //}) { (result : Bool) in
        self.goToLastCell()
        //}
    }
    
    // MARK: Keyboard
    func keyboardShow(_ frame: CGRect) {
        print("Keyboard show")
        isKeyboard = true
        self.tapCatchView.isHidden = false;
        self.trySpeakButton.isHidden = true;
        closeSpeechLayoutIfOpen()
        let dy = frame.size.height
        updateViewOffset(dy)
    }
    
    func keyboardHide() {
        print("Keyboard Hide")
        isKeyboard = false
        self.tapCatchView.isHidden = true;
        if isSpeechLayoutOpen{
            self.trySpeakButton.isHidden = true;
        }else{
            self.trySpeakButton.isHidden = false;
        }
        updateViewOffset(0)
    }
    
    func moveY(dy : CGFloat) {
        print("moveY: \(dy)")
        printHeights()
        let targetY = bottomStackViewY + dy
        if (targetY < self.frame.height - self.bottomStackView.frame.height && targetY > self.frame.height - self.bottomStackView.frame.height - self.menuOptionStackView.frame.height) {
            self.bottomStackView.frame = CGRect(x: 0, y: bottomStackViewY + dy, width: self.bottomStackView.frame.size.width, height: self.bottomStackView.frame.size.height)
            self.tableView.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.bottomStackView.frame.origin.y)
            self.menuOptionStackView.frame = CGRect(x: 0, y: self.bottomStackView.frame.origin.y + self.bottomStackView.frame.height, width: self.menuOptionStackView.frame.width, height: self.menuOptionStackView.frame.height)
        }
    }
    
    func printHeights(){
        printInfo("BottomStackView", view: self.bottomStackView)
        printInfo("TableView", view: self.tableView)
        printInfo("MenuOptionStackView", view: self.menuOptionStackView)
    }
    
    func printInfo(_ tag : String, view : UIView){
        print("\(tag) ==> Y : \(view.frame.origin.y) ,  H: \(view.frame.height) ");
    }
    
    func endY(dy : CGFloat) {
        print("endY: \(dy)")
        printHeights()
        if menuOptionStackView.y < self.frame.height - menuOptionStackView.frame.height/2.0 {
            updateViewOffset(self.menuOptionStackView.frame.height)
        }else {
            updateViewOffset(0)
        }
    }
    
    @IBAction func actionTapForKeyboard(_ tap : UITapGestureRecognizer) {
        self.inputTextField.resignFirstResponder()
    }
    
    @IBAction func actionPanForKeyboard(_ pan : UIPanGestureRecognizer) {
        self.inputTextField.resignFirstResponder()
    }
    
    // Speech Buttons related actions
    
    @IBAction func speakToggleButtonClicked(button : UIButton) {
        openSpeechLayout()
    }
    
    @IBAction func speakButtonClicked(button: UIButton) {
        if isSpeechOn{
            VTSpeechRecognizer.manager.reset()
            onSpeechListener(isStarted: false)
        }else{
            onSpeechListener(isStarted: true)
            VTSpeechRecognizer.manager.start()
        }
    }
    
    func openSpeechLayout(){
        self.inputTextField.resignFirstResponder()
        self.speakView.isHidden = false;
        self.speakButton.isSelected = true;
        isSpeechLayoutOpen = true
        self.trySpeakButton.isHidden = true;
        self.updateViewOffset(128.0)
        self.speakButtonClicked(button: self.speakButton)
        removePanGesture()
    }
    
    func closeSpeechLayoutIfOpen(){
        if isSpeechLayoutOpen{
            closeSpeechLayout()
        }
    }
    
    func addPanGesture(){
        self.panGesture.addTarget(self, action: selector)
    }
    
    func removePanGesture(){
        self.panGesture.removeTarget(self, action: selector)
    }
    
    func closeSpeechLayout(){
        addPanGesture()
        VTSpeechRecognizer.manager.stop()
        self.trySpeakButton.isHidden = false;
        self.speakView.isHidden = true;
        self.speakButton.isSelected = false;
        isSpeechLayoutOpen = false
        isSpeechOn = false;
        self.updateViewOffset(0.0)
        self.inputTextField.resignFirstResponder()
    }
    
    func onSpeechListener(isStarted: Bool) {
        DispatchQueue.main.async {
            self.isSpeechOn = isStarted;
            self.updateSpeechLayoutButton(isSpeechOn: isStarted)
        }
    }
    
    func updateSpeechLayoutButton(isSpeechOn : Bool){
        if isSpeechOn{
            let color = UIColor.init(hex: AppColors.PRIMARY)
            setImageAndTextOnSpeechView("Listening", imageName: "mic_white", backgroundColor: color)
        }else{
            setImageAndTextOnSpeechView("Tap to speak", imageName: "mic_black", backgroundColor: UIColor.white)
        }
        VCUtil.setShadowView(shadowView: self.speakButton, cornerRadius : 30.0)
    }
    
    func setImageAndTextOnSpeechView(_ text : String, imageName : String, backgroundColor : UIColor){
        self.speakLabel.text = text;
        self.speakButton.backgroundColor = backgroundColor
        self.speakButton.imageView?.highlightedImage = UIImage.init(named: imageName)
        self.speakButton.imageView?.isHighlighted = true
        self.speakButton.imageView?.image = UIImage.init(named: imageName)
    }
    
    
    @IBAction func speakCloseButtonClicked(button: UIButton) {
        closeSpeechLayout()
    }
    
    @IBAction func speakClearButtonClicked(button: UIButton) {
        self.inputTextField.text = nil
        self.textForVoice = nil
    }
    
    @IBAction func backspaceButtonClicked(button: UIButton) {
        guard let text = self.inputTextField.text else {
            return
        }
        let trimText = text.trimmingCharacters(in: .whitespaces)
        var comps = trimText.components(separatedBy: " ")
        if comps.count > 0 {
            comps.removeLast()
        }
        let joiner = " "
        self.textForVoice = comps.joined(separator: joiner)
        print("joined:" + self.textForVoice!)
        self.inputTextField.text = self.textForVoice
        //
        //        //VTSpinner.manager.start()
        //        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
        //            VTSpeechRecognizer.manager.start()
        //        }
    }
    
    func recognizeText(_ text: String, fullText : String,  isFinal: Bool) {
        if !isSpeechOn || text.isEmpty{
            return;
        }
        print("Text Recognized \(text)")
        if statusType == .NONE{
            if let voiceText = self.textForVoice {
                print("Previous Text \(voiceText)")
                let finalText = voiceText + " " + text.trimmingCharacters(in: .whitespacesAndNewlines);
                self.inputTextField.text = finalText.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            else {
                self.inputTextField.text = text
            }
        }
        let uiText = self.inputTextField.text;
        if statusType == .EMAIL{
            self.inputTextField.text = VTSpeechRecognizer.getConvertedSpeechText(fullText)
            self.textFieldValueChanged(self.inputTextField)
            self.textForVoice = uiText!
        }else if statusType == .PHONE{
            self.inputTextField.text = VTSpeechRecognizer.getConvertedSpeechText(fullText)
            self.textFieldValueChanged(self.inputTextField)
            self.textForVoice = nil;
        }else{
            self.inputTextField.text = uiText;
            self.textFieldValueChanged(self.inputTextField)
        }
        
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    
    
    
    
    
    
    
    
    // ================== Menu Items ===============
    
    
    
    func makeMenuItem() {
        DispatchQueue.main.async {
            for view in self.menuOptionStackView.arrangedSubviews {
                self.menuOptionStackView.removeArrangedSubview(view)
            }
            let item = VTMenuItem.init(self.selectedBot.isReadOutOn)
            self.menuItems?.append(item);
            if let items = self.menuItems {
                var i = 0;
                var height : CGFloat = 0;
                for menuItem in items {
                    let btn = VCUtil.getChatMenuItemButton(menuItem.title!, i: i, width: self.frame.width)
                    i += 1
                    btn.addTarget(self, action: #selector(self.menuOptionButtonClicked(button:)), for: .touchUpInside)
                    height += btn.frame.height;
                    self.menuOptionStackView.addArrangedSubview(btn)
                }
                self.menuHeightContraint.constant = height;
                self.menuOptionStackView.layoutIfNeeded()
            }
        }
    }
    
    func updateT2SOption(_ button :UIButton, menuItem : VTMenuItem){
        menuItem.isReadOutOn = !menuItem.isReadOutOn
        if menuItem.isReadOutOn{
            button.setTitle("Read Out Off", for: .normal)
        }else{
            button.setTitle("Read Out Loud", for: .normal)
        }
        VTBotInfo.getRunningInstance().isReadOutOn = menuItem.isReadOutOn;
    }
    
    func menuOptionButtonClicked(button : UIButton) {
        if let menuItem = self.menuItems?[button.tag] {
            if menuItem.menuId.isEmpty{
                updateT2SOption(button, menuItem: menuItem)
                return;
            }
            let model = self.makeUserMessageCellModel(menuItem.title!)
            self.sendSocketMessage(menuItem.makeMessageObject())
            self.cellModels.append(model)
            
            DispatchQueue.main.async {
                self.updateQuickReplyButtons()
                self.updateViewOffset(0.0)
                self.tableView.reloadData()
                self.goToLastCell()
            }
        }
    }
    
    
    func setStatusPattern(_ text : String){
        statusType = .NONE;
        if text.isEmpty{
            return;
        }
        let msg = text.lowercased();
        if msg.contains("what is"){
            if msg.contains("email address"){
                statusType = .EMAIL
            }else if msg.contains("phone") || msg.contains("mobile"){
                statusType = .PHONE
            }
        }
    }
    
    // MARK: - set message type
    func parseMessage(_ dict : [String: Any]) {
        var recipientID : String! = nil;
        if let recipient = dict["recipient"] as? [String: Any] {
            recipientID = recipient["id"] as! String
        }
        if let message = dict["message"] as? [String: Any] {
            
            let seq = message["seq"] as? Int
            let delay = message["delay"] as? Int
            
            if let quickReplies = message["quick_replies"] as? [[String: Any]] {
                self.quickReplyItems.removeAll()
                for reply in quickReplies {
                    let r = VTQuickReply(withDictionary: reply, withRecipientId: selectedBot.botID, withSeq: seq, withDelay: delay)
                    self.quickReplyItems.append(r)
                }
            }
            
            if let msg = message["text"] as? String {
                setStatusPattern(msg);
                let m = VTTextMessage(withText: msg, fromSenderId: selectedBot.botID, toRecipientId: recipientID)
                let cellModel = VTTextMessageCellModel(withTextMessage: m)
                self.cellModels.append(cellModel)
            }
            else if let attachment = message["attachment"] as? [String: Any],
                let payload = attachment["payload"] as? [String: Any] {
                
                if let templeteType = payload["template_type"] as? String {
                    if templeteType == "generic" {
                        if let elements = payload["elements"] as? [[String: Any]] {
                            var attachments = [VTAttachmentItem]()
                            for element in elements {
                                let attach = VTAttachmentItem(withDictionary: element)
                                attachments.append(attach)
                            }
                            let cellModel = VTAttachmentCellModel(withAttachments: attachments)
                            cellModel.delegate = self
                            self.cellModels.append(cellModel)
                        }
                    }
                    else if templeteType == "button" {
                        let cellModel = VTButtonTempletCellModel(withDictionary: payload)
                        cellModel.delegate = self;
                        self.cellModels.append(cellModel)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.updateQuickReplyButtons()
                self.tableView.reloadData()
                self.goToLastCell()
            }
        }
        else if let senderAction = dict["sender_action"] as? String {
            if senderAction == "typing_on" {
                self.typingCellModel.isTyping = true;
            }
            else if senderAction == "typing_off" {
                self.typingCellModel.isTyping = false;
            }
            DispatchQueue.main.async {
                self.updateQuickReplyButtons()
                self.tableView.reloadData()
                self.goToLastCell()
            }
        }
    }
    
    
    
    // MARK: Update Views
    func updateQuickReplyButtons() {
        var totalWidth : CGFloat = 10
        
        for replyButton in self.quickReplyButtons! {
            replyButton.removeFromSuperview()
        }
        self.quickReplyButtons.removeAll()
        
        for i in 0 ..< self.quickReplyItems.count {
            let menuItem = self.quickReplyItems![i]
            let btn = UIButton()
            btn.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16)
            btn.setTitleColor(UIColor(colorLiteralRed: 74.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0), for: .normal)
            btn.setTitle(menuItem.title, for: .normal)
            btn.backgroundColor = UIColor.white
            btn.layer.cornerRadius = 16
            btn.layer.borderColor = UIColor(colorLiteralRed: 74.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0).cgColor
            btn.layer.borderWidth = 1.0
            btn.tag = i
            let size = btn.titleLabel!.sizeThatFits(CGSize(width: self.frame.size.width, height: 20))
            btn.frame = CGRect(x: 0, y: 0, width: size.width + 40, height: 32)
            
            btn.addTarget(self, action: #selector(self.replyButtonClicked(_:)), for: .touchUpInside)
            
            totalWidth += size.width + 50
            
            self.quickReplyButtons.append(btn)
        }
        
        var startX : CGFloat = 10
        if totalWidth < self.frame.width {
            startX = (self.frame.width - totalWidth) / 2
        }
        
        for btn in self.quickReplyButtons {
            btn.frame = CGRect(x: startX, y: 6, width: btn.frame.width, height: 32)
            self.quickReplyScrollView.addSubview(btn)
            startX += btn.frame.size.width + 10
        }
        self.quickReplyScrollView.contentSize = CGSize(width: totalWidth, height: self.quickReplyScrollView.frame.height)
        
        if self.quickReplyItems.count == 0 {
            self.quickReplyScrollView.isHidden = true
        }
        else {
            self.quickReplyScrollView.isHidden = false
        }
        updateViewOffset(self.lastOffsetDY)
    }
    
    
    // MARK: - UITableView Delegate, DataSource
    public func numberOfSections(in tableView: UITableView) -> Int {
        if cellModels.count > 0 {
            return 2;
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return cellModels.count
        }
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let model : VTCellModel = cellModels[indexPath.row]
            return model.cellForModel(tableView)
        }
        return typingCellModel.cellForModel(tableView)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            let model : VTCellModel = cellModels[indexPath.row]
            return model.cellHeight(tableView)
        }
        return typingCellModel.cellHeight(tableView)
    }
}


